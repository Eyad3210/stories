import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sizer/sizer.dart';

import '../../config/theme/themes_app.dart';

// Local
final box = GetStorage();
const storage = FlutterSecureStorage();
const String TOKEN = 'token';
const String HAS_TOKEN = 'hasToken';

// Network
String baseUrl = "https://stories23.000webhostapp.com/api/";

// Text style
TextStyle kRegularTextStyle = TextStyle(
  fontFamily: 'MyFont',
  fontSize: 18.sp,
    fontWeight: FontWeight.w400,
  color: ThemesApp.kBlackColor,
);

TextStyle kBoldTextStyle = TextStyle(
  fontFamily: 'MyFont',
  fontSize: 18.sp,
  fontWeight: FontWeight.bold,
  color: ThemesApp.kBlackColor,
);

TextStyle kSemiBoldTextStyle = TextStyle(
  fontFamily: 'MyFont',
  fontSize: 18.sp,
  fontWeight: FontWeight.w600,
  color: ThemesApp.kBlackColor,
);

TextStyle kMediumTextStyle = TextStyle(
  fontFamily: 'MyFont',
  fontSize: 18.sp,
  fontWeight: FontWeight.w500,
  color: ThemesApp.kBlackColor,
  
);


//Functions
void showCircularProgressIndicatorInsideAlertView(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      content: SizedBox(
        width: 25.w,
        height: 15.w,
        child: const Center(
          child: CupertinoActivityIndicator(),
        ),
      ),
    ),
  );
}

bool hasToken(){
  if (box.read(HAS_TOKEN) == '1') {
    print('there is token');
    return true;
  }
  else{
    print('there is no token');
    return false;
  }
}
