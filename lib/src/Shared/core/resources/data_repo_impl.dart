import 'package:dio/dio.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:stories/src/Shared/core/resources/data_state.dart';

class DataRepoImpl {
  Future<DataState<T>> dataRepoRequest<T>(
      {required Response response,
      required Function(Map<String, dynamic>) fromJson}) async {
    try {
      if (response.statusCode == HttpStatus.ok) {
        final object = fromJson(response.data);
        return DataSuccess(object as T);
      }
      return DataFailed(DioError(
          error: response.data["message"],
          response: response,
          requestOptions: response.requestOptions,));
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
