import 'package:dio/dio.dart';
import 'package:get/get.dart' as g;

import '../util/constants.dart';

class DataService {
  final Dio _dio;

  DataService(this._dio);

  Future<Response> get(
      {required String endPoint, required bool withToken}) async {
    Response? res;
    String? value = await storage.read(key: TOKEN);
    try {
      if (withToken) {
        res = await _dio.get(
          baseUrl + endPoint,
          options: Options(
            followRedirects: false,
            validateStatus: (status) => true,
            headers: {
              'Authorization': 'Bearer $value',
              "content-type": "application/json",
              "Accept": "application/json"
            },
          ),
        );
      } else {
        res = await _dio.get(
          baseUrl + endPoint,
          options: Options(
            followRedirects: false,
            validateStatus: (status) => true,
            headers: {
              "content-type": "application/json",
              "Accept": "application/json"
            },
          ),
        );
      }
    } on DioError {
      if (res!.statusCode == 401) {
        try {
          await refreshToken();
          get(endPoint: endPoint, withToken: withToken);
        } on DioError {
          box.remove(HAS_TOKEN);
          storage.delete(key: TOKEN);
          g.Get.offAllNamed('/signInScreen');
        }
      }
      rethrow;
    }
    print(res);
    return res;
  }

  Future<Response> post({
    required String endPoint,
    required FormData? formData,
    required bool withToken,
  }) async {
    Response? res;
    String? value = await storage.read(key: TOKEN);
    try {
      if (withToken) {
        if (formData != null) {
          res = await _dio.post(
            baseUrl + endPoint,
            data: formData,
            options: Options(
              followRedirects: false,
              validateStatus: (status) => true,
              headers: {
                'Authorization': 'Bearer $value',
                "content-type": "application/json",
                "Accept": "application/json"
              },
            ),
          );
        } else {
          res = await _dio.post(
            baseUrl + endPoint,
            options: Options(
              followRedirects: false,
              validateStatus: (status) => true,
              headers: {
                'Authorization': 'Bearer $value',
                "content-type": "application/json",
                "Accept": "application/json"
              },
            ),
          );
        }
      } else {
        if (formData != null) {
          res = await _dio.post(
            baseUrl + endPoint,
            data: formData,
            options: Options(
              followRedirects: false,
              validateStatus: (status) => true,
              headers: {
                "content-type": "application/json",
                "Accept": "application/json"
              },
            ),
          );
        } else {
          res = await _dio.post(
            baseUrl + endPoint,
            options: Options(
              followRedirects: false,
              validateStatus: (status) => true,
              headers: {
                "content-type": "application/json",
                "Accept": "application/json"
              },
            ),
          );
        }
      }
    } on DioError {
      if (res!.statusCode == 401) {
        try {
          await refreshToken();
          post(endPoint: endPoint, formData: formData, withToken: withToken);
        } on DioError {
          box.remove(HAS_TOKEN);
          storage.delete(key: TOKEN);
          g.Get.offAllNamed('/signInScreen');
        }
      }
      rethrow;
    }
    print(res);
    print(res.statusCode);
    return res;
  }

  Future<Response> delete(
      {required String endPoint, required bool withToken}) async {
    Response? res;
    String? value = await storage.read(key: TOKEN);
    try {
      if (withToken) {
        res = await _dio.delete(
          baseUrl + endPoint,
          options: Options(
            followRedirects: false,
            validateStatus: (status) => true,
            headers: {
              'Authorization': 'Bearer $value',
              "content-type": "application/json",
              "Accept": "application/json"
            },
          ),
        );
      } else {
        res = await _dio.delete(
          baseUrl + endPoint,
          options: Options(
            followRedirects: false,
            validateStatus: (status) => true,
            headers: {
              "content-type": "application/json",
              "Accept": "application/json"
            },
          ),
        );
      }
    } on DioError {
      if (res!.statusCode == 401) {
        try {
          await refreshToken();
          delete(endPoint: endPoint, withToken: withToken);
        } on DioError {
          box.remove(HAS_TOKEN);
          storage.delete(key: TOKEN);
          g.Get.offAllNamed('/signInScreen');
        }
      }
      rethrow;
    }
    print(res);
    return res;
  }

  Future<void> refreshToken() async {
    Response res;
    String? value = await storage.read(key: TOKEN);
    try {
      res = await _dio.get(
        '${baseUrl}refresh',
        options: Options(
          followRedirects: false,
          validateStatus: (status) => true,
          headers: {
            'Authorization': 'Bearer $value',
            "content-type": "application/json",
            "Accept": "application/json"
          },
        ),
      );
    } on DioError {
      rethrow;
    }
    await storage.write(key: TOKEN, value: res.data["data"]["token"]);
    box.write(HAS_TOKEN, '1');
  }
}
