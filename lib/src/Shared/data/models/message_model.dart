import 'package:equatable/equatable.dart';

class GetMessageModel {
  final MessageModel data;

  GetMessageModel({
    required this.data,
  });

  factory GetMessageModel.fromJson(Map<String, dynamic> json) => GetMessageModel(
    data: MessageModel.fromJson(json["data"]),
  );
}

class MessageModel extends Equatable {
  const MessageModel({
    required this.message,
  });

  final String message;

  factory MessageModel.fromJson(Map<String, dynamic> json) =>
      MessageModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() =>
      {
        "message": message,
      };

  @override
  List<Object> get props => [message];
}
