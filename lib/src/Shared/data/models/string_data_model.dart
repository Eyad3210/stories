class GetStringDataModel {
  final String data;

  GetStringDataModel({
    required this.data,
  });

  factory GetStringDataModel.fromJson(Map<String, dynamic> json) => GetStringDataModel(
    data: json["data"],
  );
}