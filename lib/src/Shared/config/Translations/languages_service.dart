import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LanguagesService {
  final _box = GetStorage();
  final _key = 'language';

  Future<void> _saveLanguageToBox(String language) => _box.write(_key, language);

  void switchLanguage(String language) {
    _saveLanguageToBox(language);
    Get.updateLocale(Locale(language));
  }
}