import 'package:flutter/material.dart';

class ThemesApp {
  static const Color kGreenColor = Color(0xff0FBD49);
  static const Color kPurpleColor = Color(0xff554768);
  static const Color kMintColor = Color(0xff178186);
  static const Color kPinkColor = Color(0xffCD7672);
  static const Color kRedColor = Color(0xffF85353);
  static const Color kOrangeColor = Color(0xffDC8665);
  static const Color kDarkGrayColor = Color(0x2F2F2F28);
  static const Color kGrayColor = Color(0xff909090);
  static const Color kLightGrayColor = Color(0xffC0C0C0);
  static const Color kLightYellowColor = Color(0xffEEB461);
  static const Color kBlackColor = Color(0xff2F2F2F);
  static const Color kWhiteColor = Color(0xffFFFFFF);

  static final light =
      ThemeData(fontFamily: 'MyFont',);

  static final dark =
      ThemeData(fontFamily: 'MyFont',);
}
