import 'package:get/get.dart';
import 'package:flutter_bloc/flutter_bloc.dart' as bloc;
import 'package:stories/src/Features/Auth/core/args/otp_args.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Features/Auth/presentation/views/email_verify_screen.dart';
import 'package:stories/src/Features/Auth/presentation/views/manage_account_screen.dart';
import 'package:stories/src/Features/Auth/presentation/views/otp_screen.dart';
import 'package:stories/src/Features/Auth/presentation/views/profile_screen.dart';
import 'package:stories/src/Features/Packages/presentation/blocs/package_cubit.dart';

import '../../../Features/Auth/presentation/views/sign_in_screen.dart';
import '../../../Features/Auth/presentation/views/sign_up_screen.dart';
import '../../../Features/Auth/presentation/views/sign_up_second_screen.dart';
import '../../../Features/Booking/presentation/views/check_out_screen.dart';
import '../../../Features/Booking/presentation/views/running_now_booking_details_screen.dart';
import '../../../Features/Booking/presentation/views/upcoming_booking_details_screen.dart';
import '../../../Features/Kitchen/data/models/product_model.dart';
import '../../../Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import '../../../Features/Kitchen/presentation/views/cart_screen.dart';
import '../../../Features/Kitchen/presentation/views/categories_screen.dart';
import '../../../Features/Kitchen/presentation/views/product_details.dart';
import '../../../Features/Kitchen/presentation/views/products_screen.dart';
import '../../../Features/Packages/presentation/views/my_hourly_packages_screen.dart';
import '../../../Features/Sections/presentation/blocs/sections_cubit.dart';
import '../../../Features/Sections/presentation/views/confirm_reserve.dart';
import '../../../Features/Sections/presentation/views/details_unit_screen.dart';
import '../../../Features/Sections/presentation/views/sections_screen.dart';
import '../../../Features/Sections/presentation/views/themes_screen.dart';
import '../../../Features/Sections/presentation/views/units_screen.dart';
import '../../../Features/Sections/presentation/views/units_search_screen.dart';
import '../../../injector.dart';
import '../../presentation/views/home_screen.dart';

mixin AppRouter {
  static const String kHomeScreen = '/';
  static const String kSignUpScreen = '/signUpScreen';
  static const String kSignUpSecondScreen = '/signUpSecondScreen';
  static const String kEmailVerifyScreen = '/emailVerifyScreen';
  static const String kOtpScreen = '/otpScreen';
  static const String kSignInScreen = '/signInScreen';
  static const String kProfileScreen = '/profileScreen';
  static const String kManageAccountScreen = '/manageAccountScreen';
  static const String kMyHourlyPackagesScreen = '/myHourlyPackagesScreen';
  static const String kUpcomingBookingDetailsScreen =
      '/upcomingBookingDetailsScreen';
  static const String kRunningNowBookingDetailsScreen =
      '/RunningNowBookingDetailsScreen';
  static const String kCheckOutScreen = '/CheckOutScreen';

  static const String kSectionScreen = '/sectionScreen';
  static const String kThemeScreen = '/themeScreen';
  static const String kUnitScreen = '/unitScreen';
  static const String kUnitSearchScreen = '/unitSearchScreen';
  static const String kDetailsUnitScreen = '/detailsUnitScreen';
  static const String kCategoriesScreen = '/categoriesScreen';
  static const String kProductsScreen = '/productsScreen';
  static const String kDetailsProductsScreen = '/detailsProductsScreen';
  static const String kCartScreen = '/cartScreen';
  static const String kConfirmReserveScreen = '/confirmReserveScreen';

  static final pages = [
    GetPage(name: kHomeScreen, page: () => const HomeScreen()),
    GetPage(
      name: kSignUpScreen,
      page: () => const SignUpScreen(),
      transition: Transition.downToUp,
    ),
    GetPage(
      name: kSignUpSecondScreen,
      page: () => bloc.BlocProvider<AuthCubit>(
        create: (context) => injector(),
        child: const SignUpSecondScreen(),
      ),
      transition: Transition.fade,
    ),
    GetPage(
      name: kEmailVerifyScreen,
      page: () => const EmailVerifyScreen(),
      transition: Transition.fade,
    ),
    GetPage(
        name: kOtpScreen,
        page: () {
          OtpArgs args = Get.arguments;
          return bloc.BlocProvider<AuthCubit>(
            create: (context) => injector(),
            child: OtpScreen(otpArgs: args),
          );
        },
        transition: Transition.fade,
        arguments: OtpArgs),
    GetPage(
      name: kSignInScreen,
      page: () => bloc.BlocProvider<AuthCubit>(
        create: (context) => injector(),
        child: const SignInScreen(),
      ),
      transition: Transition.fade,
    ),
    GetPage(
      name: kProfileScreen,
      page: () => bloc.BlocProvider<AuthCubit>(
        create: (context) => injector(),
        child: const ProfileScreen(),
      ),
      transition: Transition.fade,
    ),
    GetPage(
      name: kManageAccountScreen,
      page: () => bloc.BlocProvider<AuthCubit>(
        create: (context) => injector(),
        child: const ManageAccountScreen(),
      ),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: kMyHourlyPackagesScreen,
      page: () => bloc.BlocProvider<PackageCubit>(
        create: (context) => injector(),
        child: const MyHourlyPackagesScreen(),
      ),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: kSectionScreen,
      page: () => bloc.BlocProvider<SectionsCubit>(
        create: (context) => injector(),
        child: const SectionScreenBody(),
      ),
      transition: Transition.rightToLeft,

      //curve: Curves.easeInOut,
      //transitionDuration: const Duration(seconds:10),
    ),
    GetPage(
        name: kThemeScreen,
        page: () {
          ThemesArguments themesArguments = Get.arguments;
          return ThemesScreenBody(
            themesArguments: themesArguments,
          );
        },
        transition: Transition.rightToLeft,
        arguments: ThemesArguments),
    GetPage(
        name: kUnitSearchScreen,
        page: () {
          UnitsSearchArguments unitsArguments = Get.arguments;
          return bloc.BlocProvider<SectionsCubit>(
            create: (context) => injector(),
            child: UnitScreenSeacrhBody(unitsSearchArguments: unitsArguments),
          );
        },
        transition: Transition.downToUp,
        arguments: UnitsSearchArguments),
    GetPage(
        name: kUnitScreen,
        page: () {
          UnitsArguments unitsArguments = Get.arguments;
          return bloc.BlocProvider<SectionsCubit>(
            create: (context) => injector(),
            child: UnitScreenBody(unitsArguments: unitsArguments),
          );
        },
        transition: Transition.downToUp,
        arguments: UnitsArguments),
    GetPage(
        name: kDetailsUnitScreen,
        page: () {
          DetailsUnitArguments detailsUnitArguments = Get.arguments;
          return bloc.BlocProvider<SectionsCubit>(
            create: (context) => injector(),
            child:
                DetailsUnitScreen(detailsUnitArguments: detailsUnitArguments),
          );
        },
        transition: Transition.rightToLeft,
        arguments: DetailsUnitArguments),
    GetPage(
      name: kCategoriesScreen,
      page: () {
        return bloc.BlocProvider<KitchenCubit>(
          create: (context) => injector(),
          child: const CategoriesScreen(),
        );
      },
      transition: Transition.rightToLeft,
    ),
    GetPage(
        name: kProductsScreen,
        page: () {
          ProductArguments productArguments = Get.arguments;
          return bloc.BlocProvider<KitchenCubit>(
            create: (context) => injector(),
            child: ProductsScreen(productArguments: productArguments),
          );
        },
        transition: Transition.rightToLeft,
        arguments: ProductArguments),
    GetPage(
        name: kDetailsProductsScreen,
        page: () {
          ProductModel productModel = Get.arguments;
          return bloc.BlocProvider<KitchenCubit>(
            create: (context) => injector(),
            child: ProductDetailsScreen(productModel: productModel),
          );
        },
        transition: Transition.rightToLeft,
        arguments: ProductModel),
    GetPage(
      name: kCartScreen,
      page: () {
        return bloc.BlocProvider<KitchenCubit>(
          create: (context) => injector(),
          child: const CartScreen(),
        );
      },
      transition: Transition.downToUp,
    ),
    GetPage(
      name: kConfirmReserveScreen,
      page: () {
        return bloc.BlocProvider<SectionsCubit>(
          create: (context) => injector(),
          child: const ConfirmReserveScreen(),
        );
      },
      transition: Transition.rightToLeft,
    ),

    GetPage(
      name: kUpcomingBookingDetailsScreen,
      page: () => const UpcomingBookingDetailsScreen(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: kRunningNowBookingDetailsScreen,
      page: () => const RunningNowBookingDetailsScreen(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: kCheckOutScreen,
      page: () => const CheckOutScreen(),
      transition: Transition.upToDown,
    ),
  ];
}
