import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../config/theme/themes_app.dart';

class CustomGradientButton extends StatelessWidget {
  final String title;
  final String? icon;
  final bool? isPrefix;
  final VoidCallback onPress;

  const CustomGradientButton({
    super.key,
    required this.title,
    this.icon,
    this.isPrefix = false,
    required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onPress,
        child: Material(
          child: Container(
            decoration: BoxDecoration(
              gradient: const LinearGradient(
                colors: [
                  ThemesApp.kMintColor,
                  ThemesApp.kPurpleColor,
                ],
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                transform: GradientRotation(261),
              ),
              borderRadius: BorderRadius.circular(1.5.h),
            ),
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.all(4.w),
              child: icon != null
                  ? !isPrefix!
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              title,
                              style: kMediumTextStyle.copyWith(
                                  fontSize: 16.sp, color: Colors.white),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            SvgPicture.asset(
                              icon!,
                              width: 3.w,
                              height: 3.h,
                            ),
                          ],
                        )
                      : Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              icon!,
                              width: 3.w,
                              height: 3.h,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              title,
                              style: kMediumTextStyle.copyWith(
                                  fontSize: 16.sp, color: Colors.white),
                            ),
                          ],
                        )
                  : Center(
                      child: Text(
                      title,
                      style: kMediumTextStyle.copyWith(
                          fontSize: 16.sp, color: Colors.white),
                    )),
            ),
          ),
        ),
      ),
    );
  }
}
