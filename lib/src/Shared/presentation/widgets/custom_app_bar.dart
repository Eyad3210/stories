import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

import '../../../Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import '../../config/router/app_router.dart';
import '../../core/util/constants.dart';
import 'package:badges/badges.dart' as b;

class CustomAppBar extends StatelessWidget {
  final String title;
  final bool? withCart;

  const CustomAppBar({
    super.key,
    required this.title,
    this.withCart = false,
  });

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(CartController());
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              overlayColor:
                  MaterialStateProperty.all<Color>(Colors.transparent),
              onTap: () {
                Get.back();
              },
              child: SvgPicture.asset(
                'assets/svgs/arrow-left.svg',
                width: 2.w,
                height: 4.h,
              ),
            ),
            SizedBox(
              width: 2.w,
            ),
            Text(
              title,
              style: kBoldTextStyle.copyWith(fontSize: 18.sp),
            ),
          ],
        ),
        withCart!
            ? GestureDetector(
                onTap: () {
                  FocusManager.instance.primaryFocus?.unfocus();
                  Get.toNamed(AppRouter.kCartScreen);
                },
                child: GestureDetector(
                  child: Obx(
                    () => b.Badge(
                      position: b.BadgePosition.custom(),
                      badgeContent: Text("${controller.badgeCount}", style: kMediumTextStyle.copyWith(fontSize: 10.sp, color: ThemesApp.kWhiteColor),),
                      badgeStyle: b.BadgeStyle(padding: EdgeInsets.all(.5.h)),
                      child: Center(
                        child: SvgPicture.asset(
                          'assets/svgs/cart.svg',
                          width: 2.w,
                          height: 4.h,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : const SizedBox()
      ],
    );
  }
}
