import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final String? icon;
  final Color borderColor;
  final Color textColor;
  final Color? iconColor;
  final Color backgroundColor;
  final VoidCallback onPress;

  const CustomButton({
    super.key,
    required this.title,
    this.icon,
    required this.onPress,
    required this.borderColor,
    required this.textColor,
    this.iconColor, required this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onPress,
        child: Material(
          child: Container(
            decoration: BoxDecoration(
              color: backgroundColor,
              border: Border.all(color: borderColor),
              borderRadius: BorderRadius.circular(1.5.h),
            ),
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.all(3.w),
              child: icon != null
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          icon!,
                          width: 3.w,
                          height: 3.h,
                          color: iconColor,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Text(
                          title,
                          style: kMediumTextStyle.copyWith(
                              fontSize: 16.sp, color: textColor),
                        ),
                      ],
                    )
                  : Center(
                      child: Text(
                      title,
                      style: kMediumTextStyle.copyWith(
                          fontSize: 16.sp, color: textColor),
                    )),
            ),
          ),
        ),
      ),
    );
  }
}
