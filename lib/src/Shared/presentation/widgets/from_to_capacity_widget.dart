import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../Features/Sections/presentation/widgets/filter_time.dart';
import '../../config/theme/themes_app.dart';

class FromToCapacityWidget extends StatelessWidget {
  const FromToCapacityWidget({
    super.key,
    required this.from,
    required this.to,
    required this.capacity,
    required this.withColor,
  });

  final String from;
  final String to;
  final String capacity;
  final bool withColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        FilterTime(
          withColor: withColor,
          widget: Text(
            from,
            style: kMediumTextStyle.copyWith(fontSize: 15.sp, color: withColor ? ThemesApp.kWhiteColor : ThemesApp.kBlackColor),
          ),
        ),
        SvgPicture.asset(
          'assets/svgs/arrow-right-black.svg',
          height: 6.w,
        ),
        FilterTime(
          withColor: withColor,
          widget: Text(
            to,
            style: kMediumTextStyle.copyWith(
                fontSize: 15.sp,
                color:
                    withColor ? ThemesApp.kWhiteColor : ThemesApp.kBlackColor),
          ),
        ),
        FilterTime(
          withColor: withColor,
          widget: Row(
            children: [
              SizedBox(
                width: 1.w,
              ),
              Text(
                capacity,
                style: kMediumTextStyle.copyWith(
                      fontSize: 15.sp,
                      color: withColor
                          ? ThemesApp.kWhiteColor
                          : ThemesApp.kBlackColor),
              ),
              SizedBox(
                width: 3.w,
              ),
              SvgPicture.asset(
                withColor ? 'assets/svgs/white-person.svg' : 'assets/svgs/borderperson.svg',
                height: 12.sp,
              ),
              SizedBox(
                width: 1.w,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
