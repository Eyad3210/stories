import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';

import '../../core/util/constants.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //
  Future<void> delete() async{
    await Get.offAllNamed(AppRouter.kSectionScreen);
    // await box.remove(TOKEN);
    // await storage.delete(key: TOKEN).then((value) => Get.toNamed(AppRouter.kSectionScreen));
  }

  @override
  void initState() {
    super.initState();
    delete();
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox()
    );
  }
}
