import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:stories/src/Features/Packages/data/repo/package_repo.dart';
import 'package:stories/src/Features/Packages/data/repo/package_repo_impl.dart';
import 'package:stories/src/Features/Packages/presentation/blocs/package_cubit.dart';
import 'package:stories/src/Shared/core/resources/data_repo_impl.dart';

import 'Features/Auth/data/repo/auth_repo.dart';
import 'Features/Auth/data/repo/auth_repo_impl.dart';
import 'Features/Auth/presentation/blocs/auth_cubit.dart';
import 'Features/Kitchen/data/repo/kitchen_repo.dart';
import 'Features/Kitchen/data/repo/kitchen_repo_impl.dart';
import 'Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import 'Features/Sections/data/repo/section_repo.dart';
import 'Features/Sections/data/repo/section_repo_impl.dart';
import 'Features/Sections/presentation/blocs/sections_cubit.dart';
import 'Shared/core/resources/data_service.dart';

final injector = GetIt.asNewInstance();

Future<void> initDependencies() async {
  injector.registerSingleton<Dio>(Dio());
  injector.registerSingleton<DataService>(DataService(injector()));
  injector.registerSingleton<DataRepoImpl>(DataRepoImpl());

  //Auth
  injector.registerSingleton<AuthRepo>(AuthRepoImpl(injector(), injector()));
  injector.registerFactory<AuthCubit>(() => AuthCubit(injector()));

  //Sections
  injector
      .registerSingleton<SectionRepo>(SectionRepoImpl(injector(), injector()));
  injector.registerFactory<SectionsCubit>(() => SectionsCubit(injector()));

  //Packages
  injector
      .registerSingleton<PackageRepo>(PackageRepoImpl(injector(), injector()));
  injector.registerFactory<PackageCubit>(() => PackageCubit(injector()));

  //kitchen
  injector
      .registerSingleton<KitchenRepo>(KitchenRepoImpl(injector(), injector()));
  injector.registerFactory<KitchenCubit>(() => KitchenCubit(injector()));
}
