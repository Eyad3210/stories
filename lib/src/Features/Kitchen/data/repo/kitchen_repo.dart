import 'package:stories/src/Features/Kitchen/data/models/categories_model.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';

import '../../../../Shared/core/resources/data_state.dart';

abstract class KitchenRepo {
  Future<DataState<GetCategoriesModel>> getCategories(
      {required int centerId, required int page});
  Future<DataState<GetProductsModel>> getProducts(
      {required int categoriesId, required int page});
}
