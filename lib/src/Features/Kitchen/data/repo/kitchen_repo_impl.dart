import 'package:stories/src/Features/Kitchen/data/models/categories_model.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';
import 'package:stories/src/Features/Kitchen/data/repo/kitchen_repo.dart';
import 'package:stories/src/Shared/core/resources/data_state.dart';

import '../../../../Shared/core/resources/data_repo_impl.dart';
import '../../../../Shared/core/resources/data_service.dart';

class KitchenRepoImpl implements KitchenRepo {
  final DataService _dataService;
  final DataRepoImpl _dataRepoImpl;

  KitchenRepoImpl(this._dataService, this._dataRepoImpl);
  @override
  Future<DataState<GetCategoriesModel>> getCategories(
      {required int centerId, required int page}) async {
    final response = await _dataService.get(
        endPoint: 'centers/$centerId/categories?page=$page', withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetCategoriesModel.fromJson);
  }

  @override
  Future<DataState<GetProductsModel>> getProducts({required int categoriesId, required int page})async {
   final response = await _dataService.get(
        endPoint: 'categories/1/products?page=$page', withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetProductsModel.fromJson);
  }
}
