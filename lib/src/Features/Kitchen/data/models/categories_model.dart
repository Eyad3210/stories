

class GetCategoriesModel {
  
  final List<CategoriesModel> categories;
  GetCategoriesModel({required this.categories});

  factory GetCategoriesModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"]["data"] as List;
    return GetCategoriesModel(
      categories: List<CategoriesModel>.from(
          dataList.map((x) => CategoriesModel.fromJson(x))),


    );
  }
}

class CategoriesModel {
    int id;
    String nameEn;
    String nameAr;
    int visible;
    String image;
    int centerId;

    CategoriesModel({
        required this.id,
        required this.nameEn,
        required this.nameAr,
        required this.visible,
        required this.image,
        required this.centerId,
    });

    factory CategoriesModel.fromJson(Map<String, dynamic> json) => CategoriesModel(
        id: json["id"],
        nameEn: json["name_en"],
        nameAr: json["name_ar"],
        visible: json["visible"],
        image: json["image"],
        centerId: json["center_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name_en": nameEn,
        "name_ar": nameAr,
        "visible": visible,
        "image": image,
        "center_id": centerId,
    };
}