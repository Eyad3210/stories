
class GetProductsModel {
  final List<ProductModel> productModel;
  GetProductsModel({required this.productModel});

  factory GetProductsModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"]["data"] as List;
    return GetProductsModel(
      productModel: List<ProductModel>.from(
          dataList.map((x) => ProductModel.fromJson(x))),
    );
  }
}

class ProductModel {
  int id;
  String nameEn;
  String nameAr;
  int price;
  int visible;
  String image;
  int categoryId;
  int quantity;
  String note;
  ProductModel({
    required this.id,
    required this.nameEn,
    required this.nameAr,
    required this.price,
    required this.visible,
    required this.image,
    required this.categoryId,
    required this.quantity,
    required this.note,
  }) ;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        nameEn: json["name_en"],
        nameAr: json["name_ar"],
        price: json["price"],
        visible: json["visible"],
        image: json["image"],
        categoryId: json["category_id"],
        quantity: json["quantity"] ?? 0,
        note: json["note"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name_en": nameEn,
        "name_ar": nameAr,
        "price": price,
        "visible": visible,
        "image": image,
        "category_id": categoryId,
        "quantity": quantity
      };
}
