import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';
import 'package:stories/src/Features/Kitchen/presentation/widgets/add_remove_botton.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../blocs/kitchen_cubit.dart';

class CartCard extends StatelessWidget {
  final CartController control;
  final ProductModel product;
  final int index;
  // final int quantity;

  CartCard({
    super.key,
    required this.control,
    required this.product,
    required this.index,
    // required this.quantity
  });
  final controller = Get.put(CartController());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.h),
      child: Container(
        decoration: BoxDecoration(
          color: ThemesApp.kWhiteColor,
          borderRadius: BorderRadius.circular(8.sp),
          boxShadow: [
            BoxShadow(
              color: ThemesApp.kBlackColor.withOpacity(0.2),
              offset: const Offset(0, 0),
              blurRadius: 8,
            ),
          ],
        ),
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
          child: Column(
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5.sp),
                    child: Image.network(
                      product.image,
                      height: 10.h,
                      width: 20.w,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(width: 2.w,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.nameEn,
                          style: kBoldTextStyle.copyWith(fontSize: 20.sp),
                        ),
                        Text(
                          product.note,
                          maxLines: 2,
                          style: kRegularTextStyle.copyWith(
                              fontSize: 15.sp, overflow: TextOverflow.ellipsis),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 2.h,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      controller.removeOneProduct(product);
                    },
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/svgs/trash.svg', width: 6.w,),
                        SizedBox(
                          width: 1.w,
                        ),
                        Text(
                          "Remove".tr,
                          style: kRegularTextStyle.copyWith(fontSize: 18.sp),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AddRemoveBotton(
                        icon: Icons.remove,
                        ontap: () {
                          controller.removeProductsFromCart(product);
                        },
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      GetBuilder<CartController>(
                        builder: (controller) {
                          return SizedBox(
                            width: 8.w,
                            child: Center(
                              child: Text(
                                product.quantity.toString(),
                                overflow: TextOverflow.ellipsis,
                                style: kBoldTextStyle.copyWith(fontSize: 18.sp),
                              ),
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        width: 3.w,
                      ),
                      AddRemoveBotton(
                        icon: Icons.add,
                        ontap: () {
                          controller.addProductToCart(product);
                          print(controller.productsMap.entries);
                        },
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
