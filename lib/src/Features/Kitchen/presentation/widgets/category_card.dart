import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/core/util/constants.dart';
import '../../data/models/categories_model.dart';

// ignore: non_constant_identifier_names
SizedBox CategoryCard(CategoriesModel categoriesModel) {
  return SizedBox(
    height: 20.h,
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14.sp),
      ),
      child: Stack(
        children: [
          Image.network(
            categoriesModel.image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 10.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(14.sp),
                  bottomRight: Radius.circular(14.sp),
                ),
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(47, 47, 47, 0),
                    Color.fromRGBO(47, 47, 47, 1),
                  ],
                  stops: [0, 1],
                ),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(74, 74, 74, 0.12),
                    offset: Offset(0, 4),
                    blurRadius: 13,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 3.h,
            left: 4.w,
            child: Row(
              children: [
                Text(categoriesModel.nameEn,
                    style: kMediumTextStyle.copyWith(
                        fontSize: 25.sp, color: Colors.white)),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
