import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';

import '../../../../Shared/core/util/constants.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({super.key, required this.productModel});
  final ProductModel productModel;
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14.sp),
      ),
      child: Stack(
        children: [
          Image.network(
            productModel.image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 15.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(14.sp),
                  bottomRight: Radius.circular(14.sp),
                ),
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(47, 47, 47, 0),
                    Color.fromRGBO(47, 47, 47, 1),
                  ],
                  stops: [0, 1],
                ),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(74, 74, 74, 0.12),
                    offset: Offset(0, 4),
                    blurRadius: 13,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 5.h,
            left: 2.w,
            right: 2.w,
            child: Text(productModel.nameEn,
                style: kMediumTextStyle.copyWith(
                    fontSize: 25.sp, color: Colors.white)),
          ),
          Positioned(
            bottom: 1.h,
            right: 2.w,
            child: Text('${'S.P'.tr} ${productModel.price}',
                style: kBoldTextStyle.copyWith(
                    fontSize: 25.sp, color: Colors.white)),
          ),
        ],
      ),
    );
  }
}
