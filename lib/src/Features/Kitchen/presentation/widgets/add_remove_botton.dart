import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

class AddRemoveBotton extends StatelessWidget {
  const AddRemoveBotton({super.key, required this.icon, required this.ontap});
  final IconData icon;
  final VoidCallback ontap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        decoration: BoxDecoration(
          color: ThemesApp.kWhiteColor,
          boxShadow: const [
            BoxShadow(
              color: ThemesApp.kLightGrayColor,
              blurRadius: 6,
              spreadRadius: 0,
              offset: Offset(0, 0),
            ),
          ],
          borderRadius: BorderRadius.circular(5.sp),
        ),
        child: Icon(
          icon,
          size: 22.sp,
        ),
      ),
    );
  }
}
