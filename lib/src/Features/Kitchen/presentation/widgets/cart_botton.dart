import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../blocs/kitchen_cubit.dart';

class CartBotton extends StatefulWidget {
  const CartBotton({super.key, required this.productModel});
  final ProductModel productModel;

  @override
  State<CartBotton> createState() => _CartBottonState();
}

class _CartBottonState extends State<CartBotton> {
  final controller = Get.put(CartController());
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GestureDetector(
        onTap: () {
          /*   setState(() {
            _isTapped = !_isTapped;
          }); */
          // ignore: unrelated_type_equality_checks

          controller.productsMap.containsKey(widget.productModel.id)
              ? controller.removeOneProduct(widget.productModel)
              : controller.addProductToCart(widget.productModel);
        },
        child: Container(
          height: 8.h,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.sp),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.topRight,
              transform: const GradientRotation(261),
              colors: controller.productsMap.containsKey(widget.productModel.id)
                  ? [
                      ThemesApp.kPinkColor,
                      ThemesApp.kOrangeColor,
                    ]
                  : [
                      ThemesApp.kMintColor,
                      ThemesApp.kPurpleColor,
                    ],
            ),
            backgroundBlendMode: BlendMode.srcOver,
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 1.5.h),
            child: Center(
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 300),
                transitionBuilder: (child, animation) => FadeTransition(
                  opacity: animation,
                  child: child,
                ),
                child:
                    controller.productsMap.containsKey(widget.productModel.id)
                        ? Row(
                            key: const ValueKey('remove'),
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/svgs/removecart.svg',
                                width: 24.sp,
                                height: 24.sp,
                                color: ThemesApp.kWhiteColor,
                              ),
                              SizedBox(width: 2.w),
                              Text(
                                "REMOVE FROM ORDER".tr,
                                style: kMediumTextStyle.copyWith(
                                  fontSize: 18.sp,
                                  color: ThemesApp.kWhiteColor,
                                ),
                              ),
                            ],
                          )
                        : Row(
                            key: const ValueKey('add'),
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/svgs/addcart.svg',
                                width: 24.sp,
                                height: 24.sp,
                                color: ThemesApp.kWhiteColor,
                              ),
                              SizedBox(width: 2.w),
                              Text(
                                "ADD TO ORDER".tr,
                                style: kMediumTextStyle.copyWith(
                                  fontSize: 18.sp,
                                  color: ThemesApp.kWhiteColor,
                                ),
                              ),
                            ],
                          ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
