import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:stories/src/Features/Kitchen/data/models/categories_model.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_state.dart';
import '../../data/repo/kitchen_repo.dart';

class KitchenCubit extends Cubit<KitchenState> {
  final KitchenRepo _kitchenRepo;
  int categoryPage = 1;
  int productPage = 1;

  KitchenCubit(this._kitchenRepo) : super(KitchenInitial());

  void loadCategories({required int centerId}) {
    if (state is CategoriesLoading) return;

    final currentState = state;

    var oldCategories = <CategoriesModel>[];
    if (currentState is CategoriesSuccess) {
      oldCategories = currentState.categoriesModel;
    }

    emit(CategoriesLoading(oldCategories, isFirstFetch: categoryPage == 1));

    _kitchenRepo
        .getCategories(centerId: centerId, page: categoryPage)
        .then((newUnits) {
      categoryPage++;

      final categories = (state as CategoriesLoading).oldCategs;
      oldCategories.addAll(newUnits.data!.categories);

      emit(CategoriesSuccess(categories));
    });
  }

  void loadProducts({required int categoryId}) {
    if (state is ProductsLoading) return;

    final currentState = state;

    var oldProducts = <ProductModel>[];
    if (currentState is ProductsSuccess) {
      oldProducts = currentState.productModel;
    }

    emit(ProductsLoading(oldProducts, isFirstFetch: categoryPage == 1));

    _kitchenRepo
        .getProducts(categoriesId: categoryId, page: categoryPage)
        .then((newUnits) {
      categoryPage++;

      final products = (state as ProductsLoading).oldProducts;
      oldProducts.addAll(newUnits.data!.productModel);

      emit(ProductsSuccess(products));
    });
  }
}

class CartController extends GetxController {
  var productsMap = {}.obs;
  //List<ProductModel> productList = [];
  RxInt badgeCount = 0.obs;

  void calculateBadge() {
    badgeCount.value = productsMap.values.length;
  }

  // ignore: non_constant_identifier_names
  void addProductToCart(ProductModel productModel) {
    if (productsMap.containsKey(productModel.id)) {
      productsMap[productModel.id].quantity += 1;
      update();
      calculateBadge();
    } else {
      
      productsMap[productModel.id] = productModel;
      productsMap[productModel.id].quantity = 1;
      update();
      calculateBadge();
    }
  }

  void removeProductsFromCart(ProductModel productModel) {
    if (productsMap.containsKey(productModel.id) &&
        productsMap[productModel.id].quantity == 1) {
      productsMap.removeWhere((key, value) => key == productModel.id);
      update();
      calculateBadge();
    } else {
      if (productsMap[productModel.id].quantity != 0) {
        productsMap[productModel.id].quantity -= 1;
      }

      update();
    }
  }

  // ignore: non_constant_identifier_names
  void removeOneProduct(ProductModel productModel) {
    productsMap.removeWhere((key, value) => key == productModel.id);
    calculateBadge();
  }

  get productSubTotal =>
      productsMap.entries.map((e) => e.value.price * e.value).toList();

  get total => productsMap.entries
      .map((e) => e.value.price * e.value)
      .toList()
      .reduce((value, element) => value + element);

  int quantity() {
    if (productsMap.isEmpty) {
      return 0;
    } else {
      return productsMap.entries
          .map((e) => e.value)
          .toList()
          .reduce((value, element) => value + element);
    }
  }
}


/* List<Student> students = [
    Student(name: "John", rollno: "12", age: "26", marks: [23, 45, 35]),
    Student(name: "Krishna", rollno: "12", age: "26", marks: [23, 45, 35]),
    Student(name: "Rahul", rollno: "12", age: "26", marks: [23, 45, 35])
];
     
var studentsmap = students.map((e){
        return {
                "name": e.name, 
                "rollno": e.rollno, 
                "age": e.age, 
                "marks": e.marks
            };
    }).toList();
print(studentsmap); */
/* 
class CartController extends GetxController {
  var productsMap = {}.obs;
  var packageList = [];
  List<ProductModel> productList = [];

  void addProductToCart(ProductModel product) {
    if (productsMap.containsKey(product)) {
      productsMap[product] += 1;
      for (int i = 0; i < productList.length; i++) {
        if (productList[i].id == product.id) {
          int help;
          help = productList[i].quantity;
          help += 1;
          productList[i].quantity = help;
        }
      }

    } else {
      productsMap[product] = 1;
      productList.add(ProductModel(id: product.id,quantity: 1,categoryId: 1,image: "",nameAr: "",nameEn: "",price: 0,visible: 1));
    }
  }

  void removeProductsFromCart(ProductModel product) {
    if(productsMap.isEmpty) {
      return;
    }  
    if (productsMap.containsKey(product) && productsMap[product] == 1) {
      productsMap.removeWhere((key, value) => key == product);

          productList.removeWhere((value) => value==product);
         
          print(productList);
       

    } else {
      productsMap[product] -= 1;
       for (int i = 0; i < productList.length; i++) {
        if (productList[i].id == product.id) {
          int help;
          help = productList[i].quantity;
          help -= 1;
          productList[i].quantity = help;
        }
      }

    }
  }

  void removeOneProduct(ProductModel product) {
    productsMap.removeWhere((key, value) => key == product);
  } 
/* 
  void clearAllProducts() {
    Get.defaultDialog(
      title: "Clean Products",
      titleStyle: const TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.bold,
      ),
      middleText: 'Are you sure you need clear products',
      middleTextStyle: const TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.bold,
      ),
      backgroundColor: Colors.grey[300],
      radius: 10,
      textCancel: " No ",
      cancelTextColor: Colors.white,
      textConfirm: " YES ",
      confirmTextColor: Colors.white,
      onCancel: () {
        Get.toNamed(Routes.cartScreen);
      },
      onConfirm: () {
        productsMap.clear();
        productsList.clear();
        print(productsList);

        badgeCount.value = 0;
        Get.back();
      },
      buttonColor: Get.isDarkMode ? Colors.red : mainColor,
    );
  } */

  get productSubTotal =>
      productsMap.entries.map((e) => e.key.price * e.value).toList();

  get total => productsMap.entries
      .map((e) => e.key.price * e.value)
      .toList()
      .reduce((value, element) => value + element);


  int quantity() {
    if (productsMap.isEmpty) {
      return 0;
    } else {
      return productsMap.entries
          .map((e) => e.value)
          .toList()
          .reduce((value, element) => value + element);
    }
  }
}
 */