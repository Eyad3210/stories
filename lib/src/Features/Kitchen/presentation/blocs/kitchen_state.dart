import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Kitchen/data/models/categories_model.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';

abstract class KitchenState extends Equatable {
  const KitchenState();

  @override
  List<Object?> get props => [];
}

class KitchenInitial extends KitchenState {}

class CategoriesLoading extends KitchenState {
  final List<CategoriesModel> oldCategs;
  final bool isFirstFetch;

  const CategoriesLoading(this.oldCategs, {this.isFirstFetch=false});
}
class CategoriesSuccess extends KitchenState {
  final List<CategoriesModel> categoriesModel;

  const CategoriesSuccess(this.categoriesModel);
    @override
  List<Object> get props => [categoriesModel];
}
class CategoriesFailure extends KitchenState {
   final DioError error;

  const CategoriesFailure(this.error);

  @override
  List<Object> get props => [error];
}

//////////

class ProductsLoading extends KitchenState {
  final List<ProductModel> oldProducts;
  final bool isFirstFetch;

  const ProductsLoading(this.oldProducts, {this.isFirstFetch=false});
}
class ProductsSuccess extends KitchenState {
  final List<ProductModel> productModel;

  const ProductsSuccess(this.productModel);
    @override
  List<Object> get props => [productModel];
}
class ProductFailure extends KitchenState {
   final DioError error;

  const ProductFailure(this.error);

  @override
  List<Object> get props => [error];
}
