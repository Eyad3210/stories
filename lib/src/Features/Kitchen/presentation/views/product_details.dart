import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/data/models/product_model.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import 'package:stories/src/Features/Kitchen/presentation/widgets/cart_botton.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({
    super.key,
    required this.productModel,
  });
  final ProductModel productModel;
  @override
  State<ProductDetailsScreen> createState() => _ProductDetailsScreenState();
}

final controller = Get.put(CartController());
final textController = TextEditingController();

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  void initState() {
    if (controller.productsMap.containsKey(widget.productModel.id)) {
      if (controller.productsMap[widget.productModel.id].note != null) {
        textController.text =
            controller.productsMap[widget.productModel.id].note;
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    textController.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
          child: CartBotton(productModel: widget.productModel),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
                child: CustomAppBar(
                  title: widget.productModel.nameEn,
                  withCart: true,
                ),
              ),
              Image.network(
                widget.productModel.image,
                height: 25.h,
                fit: BoxFit.cover,
                width: double.infinity,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.productModel.nameEn,
                          style: kBoldTextStyle.copyWith(
                              fontSize: 20.sp, color: ThemesApp.kBlackColor),
                        ),
                        Text(
                          "${'S.P'.tr} ${widget.productModel.price}",
                          style: kBoldTextStyle.copyWith(
                              fontSize: 20.sp, color: ThemesApp.kBlackColor),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Wrap(
                      children: [
                        Text(
                          'Description here.. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                          softWrap: true,
                          style: kRegularTextStyle.copyWith(fontSize: 12.sp),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Center(
                      child: Container(
                        width: 50.w,
                        height: 0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey.withOpacity(0.5),
                            style: BorderStyle.solid,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      "Add Your Notes".tr,
                      style: kBoldTextStyle.copyWith(
                        fontSize: 20.sp,
                        color: ThemesApp.kBlackColor,
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Container(
                      padding: EdgeInsets.all(2.h),
                      height: 15.h,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border:
                            Border.all(width: 0.5, color: ThemesApp.kGrayColor),
                        borderRadius: BorderRadius.circular(8.sp),
                      ),
                      child: TextField(
                        controller: textController,
                        onChanged: (value) {
                          widget.productModel.note = value;
                          controller.productsMap
                                  .containsKey(widget.productModel.id)
                              ? controller.productsMap[widget.productModel.id]
                                  .note = widget.productModel.note
                              : widget.productModel.note = value;
                        },

                        decoration: InputDecoration(
                          hintText: "Your Notes Here".tr,
                          hintStyle: kRegularTextStyle.copyWith(
                              fontSize: 16.sp, color: Colors.grey),
                          border: InputBorder.none,
                        ),

                        style: kRegularTextStyle.copyWith(fontSize: 16.sp),
                        maxLines: null,
                      ),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
