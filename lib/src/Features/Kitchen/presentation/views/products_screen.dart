import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_state.dart';
import '../../../../Shared/config/router/app_router.dart';
import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../data/models/product_model.dart';
import '../blocs/kitchen_cubit.dart';
import '../widgets/product_card.dart';

class ProductArguments {
  final int categoryId;

  ProductArguments(this.categoryId);
}

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key, required this.productArguments});
  final ProductArguments productArguments;

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

final scrollController = ScrollController();

class _ProductsScreenState extends State<ProductsScreen> {
  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<KitchenCubit>(context)
              .loadProducts(categoryId: widget.productArguments.categoryId);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<KitchenCubit>(context)
        .loadProducts(categoryId: widget.productArguments.categoryId);
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(
                title: 'Kitchen Menu'.tr,
                withCart: true,
              ),
            ),
            Expanded(
              child: BlocBuilder<KitchenCubit, KitchenState>(
                builder: (context, state) {
                  if (state is ProductsLoading && state.isFirstFetch) {
                    return const Center(child: CircularProgressIndicator());
                  }

                  List<ProductModel> products = [];
                  bool isLoading = false;

                  if (state is ProductsLoading) {
                    products = state.oldProducts;
                    isLoading = true;
                  } else if (state is ProductsSuccess) {
                    products = state.productModel;
                  }

                  return Column(
                    children: [
                      Expanded(
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return GridView.builder(
                                controller: scrollController,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 3.w, vertical: 3.h),
                                itemCount:
                                    products.length + (isLoading ? 1 : 0),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio:
                                      constraints.biggest.aspectRatio * 2 / 1.3,
                                  mainAxisSpacing: 8,
                                  crossAxisSpacing: 10,
                                ),
                                itemBuilder: (context, index) {
                                  if (index < products.length) {
                                    return InkWell(
                                        onTap: () {
                                          Get.toNamed(
                                              AppRouter.kDetailsProductsScreen,
                                              arguments: products[index]);
                                        },
                                        child: ProductCard(
                                          productModel: products[index],
                                        ));
                                  } else {
                                    Timer(const Duration(milliseconds: 30), () {
                                      scrollController.jumpTo(scrollController
                                          .position.maxScrollExtent);
                                    });

                                    return const Center(
                                      child: CircularProgressIndicator(
                                        color: ThemesApp.kPurpleColor,
                                      ),
                                    );
                                  }
                                });
                          },
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
