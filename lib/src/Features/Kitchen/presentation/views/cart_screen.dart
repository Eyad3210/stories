import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/presentation/widgets/cart_card.dart';
import 'package:stories/src/Features/Sections/presentation/widgets/main_appbar.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../../Sections/presentation/widgets/custom_bottombar.dart';
import '../blocs/kitchen_cubit.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

final controller = Get.put(CartController());

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: Obx(
          () => controller.productsMap.isEmpty
              ? CustomBottomBar(
                  title: "KITCHEN MENU".tr,
                  ontap: () {},
                )
              : CustomBottomBar(
                  title: "CONFIRM ORDER".tr,
                  ontap: () {},
                ),
        ),
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(
                title: 'Kitchen Order'.tr,
                withCart: false,
              ),
            ),
            Expanded(
              child: Obx(
                () {
                  if (controller.productsMap.isEmpty) {
                    return const EmptyCart();
                  } else {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return CartCard(
                          control: controller,
                          index: index,
                          product:
                              controller.productsMap.values.toList()[index],
                        );
                      },
                      itemCount: controller.productsMap.length,
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EmptyCart extends StatelessWidget {
  const EmptyCart({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/svgs/emptycart.svg',
            width: 30.w,
            color: ThemesApp.kPinkColor,
          ),
          Text(
            "There is no orders yet!".tr,
            style: kBoldTextStyle.copyWith(fontSize: 22.sp),
          ),
          Text(
            "Enter the kitchen menu and discover our delicious\nvarieties of drinks and desserts".tr,
            style: kRegularTextStyle.copyWith(
              fontSize: 16.sp,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
