import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/data/models/categories_model.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_state.dart';
import 'package:stories/src/Features/Kitchen/presentation/views/products_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_app_bar.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../widgets/category_card.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({super.key});

  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

final scrollController = ScrollController();

void setupScrollController(context) {
  scrollController.addListener(() {
    if (scrollController.position.atEdge) {
      if (scrollController.position.pixels != 0) {
        BlocProvider.of<KitchenCubit>(context).loadCategories(centerId: 1);
      }
    }
  });
}

final controller = Get.put(CartController());

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<KitchenCubit>(context).loadCategories(centerId: 1);
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(
                title: 'Kitchen Menu'.tr,
                withCart: true,
              ),
            ),
            Expanded(
              child: BlocBuilder<KitchenCubit, KitchenState>(
                builder: (context, state) {
                  if (state is CategoriesLoading && state.isFirstFetch) {
                    return const Center(child: CircularProgressIndicator());
                  }

                  List<CategoriesModel> categs = [];
                  bool isLoading = false;

                  if (state is CategoriesLoading) {
                    categs = state.oldCategs;
                    isLoading = true;
                  } else if (state is CategoriesSuccess) {
                    categs = state.categoriesModel;
                  }
                  return Column(
                    children: [
                      Image.asset(
                        'assets/png/desks.png',
                        height: 25.h,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                      Expanded(
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return ListView.separated(
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  height: 1.h,
                                );
                              },
                              padding: EdgeInsets.symmetric(
                                  horizontal: 3.w, vertical: 3.h),
                              controller: scrollController,
                              shrinkWrap: true,
                              itemCount: categs.length + (isLoading ? 1 : 0),
                              itemBuilder: (context, index) {
                                if (index < categs.length) {
                                  return InkWell(
                                      onTap: () {
                                        Get.toNamed(AppRouter.kProductsScreen,
                                            arguments: ProductArguments(
                                                categs[index].id));
                                      },
                                      child: CategoryCard(categs[index]));
                                } else {
                                  Timer(
                                    const Duration(milliseconds: 30),
                                    () {
                                      scrollController.jumpTo(scrollController
                                          .position.maxScrollExtent);
                                    },
                                  );
                                  return const Center(
                                    child: CircularProgressIndicator(
                                      color: ThemesApp.kPurpleColor,
                                    ),
                                  );
                                }
                              },
                            );
                          },
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
