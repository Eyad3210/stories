import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stories/src/Features/Sections/data/models/unit_model.dart';
import 'package:stories/src/Features/Sections/data/repo/section_repo.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_state.dart';

import '../../../../Shared/core/resources/data_state.dart';

class SectionsCubit extends Cubit<SectionsState> {

  
  final SectionRepo _sectionRepo;
  int page = 1;
  List<int> hourCost = [];

  SectionsCubit(this._sectionRepo) : super(SectionsInitial());

  Future<void> getSections({required int centerId}) async {
    emit(SectionsLoading());
    final dataState = await _sectionRepo.getSections(centerId: centerId);
    if (dataState is DataSuccess) {
      emit(SectionsSuccess(dataState.data!));
    }
    if (dataState is DataFailed) {
      emit(SectionsFailure(dataState.error!));
    }
  }

  void loadUnits({required int sectionId}) {
    if (state is UnitLoading) return;

    final currentState = state;

    var oldUnits = <UnitModel>[];
    if (currentState is UnitSuccess) {
      oldUnits = currentState.listUnit;
    }

    emit(UnitLoading(oldUnits, isFirstFetch: page == 1));

    _sectionRepo.getUnits(sectionId: sectionId, page: page).then((newUnits) {
      page++;

      final units = (state as UnitLoading).oldUnit;
      oldUnits.addAll(newUnits.data!.units);

      emit(UnitSuccess(units));
    });
  }
}
