import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Sections/data/models/section_model.dart';

import '../../data/models/unit_model.dart';

abstract class SectionsState extends Equatable {
  const SectionsState();

  @override
  List<Object?> get props => [];
}

class SectionsInitial extends SectionsState {}

class SectionsLoading extends SectionsState {}

class SectionsSuccess extends SectionsState {
  final GetSectionsModel sectionModel;

  const SectionsSuccess(this.sectionModel);
  @override
  List<Object> get props => [sectionModel];
}

class SectionsFailure extends SectionsState {
  final DioError error;

  const SectionsFailure(this.error);

  @override
  List<Object> get props => [error];
}

//////////
///
class UnitLoading extends SectionsState {
  final List<UnitModel> oldUnit;
  final bool isFirstFetch;

const UnitLoading(this.oldUnit, {this.isFirstFetch=false});}

class UnitSuccess extends SectionsState {
  final  List<UnitModel> listUnit;

  const UnitSuccess( this.listUnit);
  @override
  List<Object> get props => [listUnit];
}

class UnitFailure extends SectionsState {
  final DioError error;

  const UnitFailure(this.error);

  @override
  List<Object> get props => [error];
}
