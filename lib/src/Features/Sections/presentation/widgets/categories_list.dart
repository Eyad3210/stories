import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/presentation/views/themes_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/core/util/constants.dart';
import '../../../../Shared/config/theme/themes_app.dart';
import '../blocs/sections_cubit.dart';
import '../blocs/sections_state.dart';

// ignore: must_be_immutable
class CategoriesList extends StatefulWidget {
  const CategoriesList({super.key});

  @override
  State<CategoriesList> createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList> {
  List<Color> colors = [
    ThemesApp.kPurpleColor,
    ThemesApp.kPinkColor,
    ThemesApp.kOrangeColor
  ];
  List<String> images = [
    'assets/png/desks.png',
    'assets/png/studios.png',
    'assets/png/room.png'
  ];
  List<String> sections = ['Desks'.tr, 'Studios'.tr, 'Meeting\nRoom'.tr];
  List<String> description = [
    'Reserve your DESK to unleash\nyour creativity anywhere'.tr,
    'Reserve your STUDIO to unleash\nyour creativity anywhere'.tr,
    'Reserve your ROOM to unleash\nyour creativity anywhere'.tr
  ];
  @override
  void initState() {
    BlocProvider.of<SectionsCubit>(context).getSections(centerId: 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SectionsCubit, SectionsState>(
      builder: (context, state) {
        if (state is SectionsLoading) {
          return const Center(
              child: CircularProgressIndicator(
            color: ThemesApp.kPurpleColor,
          ));
        } else if (state is SectionsFailure) {
          return const Text("Error faliure");
        } else if (state is SectionsSuccess) {
          return ListView.separated(
            separatorBuilder: (context, index) {
              return SizedBox(
                height: 1.h,
              );
            },
            padding: EdgeInsets.symmetric(vertical: 2.h),
            itemCount: 3,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Get.toNamed(
                    AppRouter.kThemeScreen,
                    arguments: ThemesArguments(
                        state.sectionModel.sections[index].sectionImages,
                        state.sectionModel.sections[index].hourCost,
                        state.sectionModel.sections[index].title),
                  );
                },
                child: Stack(
                  children: [
                    LayoutBuilder(
                      builder:
                          (BuildContext context, BoxConstraints constraints) {
                        return SizedBox(
                          width: constraints.maxWidth,
                          height: constraints.maxWidth / 1.8,
                          child: Image.asset(
                            images[index],
                            fit: BoxFit.cover,
                          ),
                        );
                      },
                    ),
                    LayoutBuilder(
                      builder:
                          (BuildContext context, BoxConstraints constraints) {
                        return SizedBox(
                          width: constraints.maxWidth,
                          height: constraints.maxWidth / 1.8,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomCenter,
                                colors: [
                                  colors[index],
                                  ThemesApp.kWhiteColor.withOpacity(0.01),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    LayoutBuilder(
                      builder:
                          (BuildContext context, BoxConstraints constraints) {
                        return SizedBox(
                          width: constraints.maxWidth,
                          height: constraints.maxWidth / 1.8,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  colors[index].withOpacity(0.5),
                                  Colors.white.withOpacity(0.01)
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    Positioned(
                      left: 3.w,
                      top: 7.h,
                      child: SizedBox(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              sections[index],
                              style: kBoldTextStyle.copyWith(
                                  fontSize: 30.sp,
                                  color: ThemesApp.kWhiteColor,
                                  height: .7),
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Text(
                              description[index],
                              style: kRegularTextStyle.copyWith(
                                  fontSize: 15.sp,
                                  color: ThemesApp.kWhiteColor,
                                  height: .7),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        }
        return const Center(child: Text("Error"));
      },
    );
  }
}
