import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

class SlideBarCard extends StatelessWidget {
  const SlideBarCard({super.key, required this.title, required this.icon});
  final String title;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(2.w),
        decoration: BoxDecoration(
          color: ThemesApp.kWhiteColor,
          borderRadius: BorderRadius.circular(16.sp),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(74, 74, 74, 0.12),
              blurRadius: 8,
              offset: Offset(0, 0),
            ),
          ],
        ),
        child: Column(
          children: [
            SvgPicture.asset(
              icon,
              width: 7.w,
            ),
            SizedBox(
              height: 1.h,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: ThemesApp.kBlackColor,
                fontSize: 8.sp,
                decoration: TextDecoration.none,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
