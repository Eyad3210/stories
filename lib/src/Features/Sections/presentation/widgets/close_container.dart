import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';


class CloseContainer extends StatelessWidget {
  const CloseContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 10.w,
      height: 0.5.h,
      decoration: BoxDecoration(
          color: ThemesApp.kPurpleColor, borderRadius: BorderRadius.circular(20.sp)),
    );
  }
}
