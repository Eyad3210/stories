import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Kitchen/presentation/blocs/kitchen_cubit.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:badges/badges.dart' as b;
import 'package:stories/src/Shared/config/theme/themes_app.dart';

import '../../../../Shared/core/util/constants.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MainAppBar(
      {super.key,
      required this.title,
      required this.onPressed,
      required this.withCart});
  final String title;
  final VoidCallback onPressed;
  final bool withCart;

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(CartController());
    return AppBar(
      title: Text(
        title,
        style: kBoldTextStyle.copyWith(fontSize: 22.sp),
      ),
      leading: IconButton(
        onPressed: onPressed,
        icon: SvgPicture.asset(
          'assets/svgs/arrow-left.svg',
          width: 6.w,
          height: 4.h,
        ),
      ),
      actions: withCart
          ? [
              GestureDetector(
                onTap: () {
                  FocusManager.instance.primaryFocus?.unfocus();
                  Get.toNamed(AppRouter.kCartScreen);
                },
                child: Padding(
                  padding: EdgeInsets.only(right: 3.w),
                  child: GestureDetector(
                    child: Obx(
                      () => b.Badge(
                        position: b.BadgePosition.custom(),
                        badgeContent: Text("${controller.badgeCount}"),
                        child: Center(
                          child: SvgPicture.asset(
                            'assets/svgs/cart.svg',
                            width: 2.w,
                            height: 4.h,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ]
          : [],
      backgroundColor: ThemesApp.kWhiteColor,
      elevation: 0,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
