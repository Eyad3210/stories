import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/presentation/widgets/slidebar_card.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import 'close_container.dart';

// ignore: must_be_immutable
class HomeAppBar extends StatelessWidget implements PreferredSizeWidget {
  const HomeAppBar({super.key});
  @override
  Widget build(BuildContext context) {
    var size=MediaQuery.of(context).size;
        return AppBar(
      backgroundColor: ThemesApp.kWhiteColor,
      elevation: 5,
      centerTitle: true,
      title: Image.asset(
        'assets/png/stories-logo2.png',
        
        width: size.width/4,
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: InkWell(
            onTap: () {
              generalDialog(context);
            },
            child: SvgPicture.asset(
              'assets/svgs/menu.svg',
              width: size.width/15,
            ),
          ),
        ),
      ],
    );
  }

  Future<Object?> generalDialog(BuildContext context) {
    return showGeneralDialog(
              context: context,
              barrierDismissible: true,
              transitionDuration: const Duration(milliseconds: 1000),
              barrierLabel: MaterialLocalizations.of(context).dialogLabel,
              barrierColor: Colors.transparent,
              pageBuilder: (context, _, __) {
                return GestureDetector(
                  onVerticalDragUpdate: (details) {
                    if (details.primaryDelta! < -10) {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 12.h,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: ThemesApp.kWhiteColor,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(32.sp),
                              bottomRight: Radius.circular(32.sp),
                            ),
                          ),
                          width: double.infinity,
                          child: Stack(
                            children: [
                              Positioned(
                                left: 0,
                                child: SvgPicture.asset(
                                  'assets/svgs/circle2.svg',
                                  width: 45.w,
                                ),
                              ),
                              Positioned(
                                right: 2.w,
                                bottom: 2.h,
                                child: SvgPicture.asset(
                                  'assets/svgs/circle1.svg',
                                  width: 30.w,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 2.w, right: 2.w, top: 2.h),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        SlideBarCard(
                                          title: "Special\nOffers".tr,
                                          icon: 'assets/svgs/verify.svg',
                                        ),
                                        SizedBox(
                                          width: 1.w,
                                        ),
                                        SlideBarCard(
                                          title: "Booking\nPackeges".tr,
                                          icon: 'assets/svgs/ticket-star.svg',
                                        ),
                                        SizedBox(
                                          width: 2.w,
                                        ),
                                        SlideBarCard(
                                            title: "Internet\nBundles".tr,
                                            icon:
                                                'assets/svgs/wifi-package.svg'),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Row(
                                      children: [
                                        SlideBarCard(
                                            title: "Notification".tr,
                                            icon:
                                                'assets/svgs/notification.svg'),
                                        SizedBox(
                                          width: 2.w,
                                        ),
                                        SlideBarCard(
                                            title: "Kitchen".tr,
                                            icon: 'assets/svgs/coffee.svg'),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 4.h,
                                    ),
                                    const CloseContainer(),
                                    SizedBox(
                                      height: 4.h,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              transitionBuilder:
                  (context, animation, secondaryAnimation, child) {
                return SlideTransition(
                  position: CurvedAnimation(
                    parent: animation,
                    curve: Curves.fastLinearToSlowEaseIn,
                    reverseCurve: Curves.fastOutSlowIn,
                  ).drive(
                    Tween<Offset>(
                      begin: const Offset(0, -1),
                      end: const Offset(0, 0),
                    ),
                  ),
                  child: child,
                );
              },
            );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

/* 
showGeneralDialog(
                context: context,
                barrierDismissible: true,
                transitionDuration: const Duration(milliseconds: 1000),
                barrierLabel: MaterialLocalizations.of(context).dialogLabel,
                barrierColor: Colors.transparent,
                pageBuilder: (context, _, __) {
                  return GestureDetector(
                    onVerticalDragUpdate: (details) {
                      if (details.primaryDelta! < -10) {
                        Navigator.of(context).pop();
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 12.h,
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: ThemesApp.kWhiteColor,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(32.sp),
                                bottomRight: Radius.circular(32.sp),
                              ),
                            ),
                            width: double.infinity,
                            child: Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  child: SvgPicture.asset(
                                    'assets/svgs/circle2.svg',
                                    width: 45.w,
                                  ),
                                ),
                                Positioned(
                                  right: 2.w,
                                  bottom: 2.h,
                                  child: SvgPicture.asset(
                                    'assets/svgs/circle1.svg',
                                    width: 30.w,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 2.w, right: 2.w, top: 2.h),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          SlideBarCard(
                                            title:
                                                "Special\nOffers".tr,
                                            icon: 'assets/svgs/verify.svg',
                                          ),
                                          SizedBox(
                                            width: 1.w,
                                          ),
                                          SlideBarCard(
                                            title:
                                                "Booking\nPackeges".tr,
                                            icon: 'assets/svgs/ticket-star.svg',
                                          ),
                                          SizedBox(
                                            width: 2.w,
                                          ),
                                          SlideBarCard(
                                              title:
                                                  "Internet\nBundles".tr,
                                              icon:
                                                  'assets/svgs/wifi-package.svg'),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 2.h,
                                      ),
                                      Row(
                                        children: [
                                          SlideBarCard(
                                              title: "Notification".tr,
                                              icon:
                                                  'assets/svgs/notification.svg'),
                                          SizedBox(
                                            width: 2.w,
                                          ),
                                          SlideBarCard(
                                              title: "Kitchen".tr,
                                              icon: 'assets/svgs/coffee.svg'),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 4.h,
                                      ),
                                      const CloseContainer(),
                                      SizedBox(
                                        height: 4.h,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                transitionBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return SlideTransition(
                    position: CurvedAnimation(
                      parent: animation,
                      curve: Curves.fastLinearToSlowEaseIn,
                      reverseCurve: Curves.fastOutSlowIn,
                    ).drive(
                      Tween<Offset>(
                        begin: const Offset(0, -1),
                        end: const Offset(0, 0),
                      ),
                    ),
                    child: child,
                  );
                },
              ); */