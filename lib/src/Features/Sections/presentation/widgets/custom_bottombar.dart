import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_gradient_button.dart';

class CustomBottomBar extends StatelessWidget {
  const CustomBottomBar(
      {super.key,
      required this.ontap,
      required this.title,});

  final VoidCallback ontap;
  final String title;
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 2.w, right: 2.w),
      child: Container(
        height: 16.h,
        decoration: BoxDecoration(
          color: ThemesApp.kWhiteColor,
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, -12),
              blurRadius: 12,
              color: Color.fromRGBO(74, 74, 74, 0.12),
            )
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.sp),
            topRight: Radius.circular(12.sp),
          ),
        ),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomGradientButton(title: title, onPress: ontap),
            ],
          ),
        ),
      ),
    );
  }
}
