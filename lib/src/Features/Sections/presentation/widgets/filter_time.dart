import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../../../Shared/config/theme/themes_app.dart';

class FilterTime extends StatelessWidget {
  const FilterTime({super.key, required this.widget, required this.withColor});
  final Widget widget;
  final bool withColor;


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
       border: Border.all(width: !withColor? 1:0,color: ThemesApp.kPurpleColor),
        borderRadius: BorderRadius.circular(19.sp),
       gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
          withColor?  ThemesApp.kPurpleColor:Colors.white,
          withColor?  ThemesApp.kMintColor:Colors.white,
          ],
          stops: const [
            0,
            1,
          ],
        ),
        backgroundBlendMode: BlendMode.srcOver,
      ),
      child: Padding(
        padding:  EdgeInsets.symmetric(vertical: 1.h,horizontal: 1.w),
        child: FittedBox(
            child: widget),
      ),
    );
  }
}
