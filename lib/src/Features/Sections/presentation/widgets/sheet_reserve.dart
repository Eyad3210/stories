import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/presentation/views/units_search_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/core/util/constants.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_gradient_button.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import 'close_container.dart';

void reserveBottomSheet(BuildContext context, int price, String themeName) {
  showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(left: 2.w, right: 2.w),
        child: Container(
          height: 30.h,
          decoration: BoxDecoration(
            color: ThemesApp.kWhiteColor,
            boxShadow: const [
              BoxShadow(
                offset: Offset(0, -12),
                blurRadius: 12,
                color: Color.fromRGBO(74, 74, 74, 0.12),
              )
            ],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12.sp),
              topRight: Radius.circular(12.sp),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: .5.h,
              ),
              const CloseContainer(),
              SizedBox(
                height: .5.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.sp),
                    border: Border.all(color: ThemesApp.kPurpleColor, width: 1),
                  ),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/svgs/calender-purpel.svg',
                        width: 6.w,
                        color: ThemesApp.kGrayColor,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        'From'.tr,
                        style: kMediumTextStyle.copyWith(
                            fontSize: 16.sp, color: ThemesApp.kGrayColor),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      SvgPicture.asset(
                        'assets/svgs/arrow-right-white.svg',
                        width: 6.w,
                        color: ThemesApp.kGrayColor,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        'To'.tr,
                        style: kMediumTextStyle.copyWith(
                            fontSize: 16.sp, color: ThemesApp.kGrayColor),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.sp),
                    border: Border.all(color: ThemesApp.kPurpleColor, width: 1),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/svgs/persons.svg',
                        width: 6.w,
                        color: ThemesApp.kGrayColor,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        'Number of persons'.tr,
                        style: kMediumTextStyle.copyWith(
                            fontSize: 16.sp, color: ThemesApp.kGrayColor),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: CustomGradientButton(
                  title: 'Search Available Deskes'.tr,
                  onPress: () {
                    Get.toNamed(AppRouter.kUnitSearchScreen,
                        arguments: UnitsSearchArguments(price, themeName));
                  },
                ),
              ),
              SizedBox(
                height: .5.h,
              ),
            ],
          ),
        ),
      );
    },
  );
}
