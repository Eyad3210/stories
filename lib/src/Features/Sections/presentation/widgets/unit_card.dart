import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../data/models/unit_model.dart';

Container unitsWidget(UnitModel unitModel, BuildContext context, int price) {
  return Container(
    margin: EdgeInsets.only(left: 2.w, right: 2.w, top: 2.h),
    height: 25.h,
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.sp),
      ),
      child: Stack(
        children: [
          Image.network(
            unitModel.image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 15.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12.sp),
                  bottomRight: Radius.circular(12.sp),
                ),
                gradient: const LinearGradient(
                    colors: [
                      Color.fromRGBO(205, 118, 114, 0),
                      Color.fromRGBO(205, 118, 114, 1),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0, 6]),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(74, 74, 74, 0.12),
                    offset: Offset(0, 4),
                    blurRadius: 13,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 2.h,
            right: 4.w,
            child: SvgPicture.asset(
              'assets/svgs/white-heart.svg',
              width: 7.w,
            ),
          ),
          Positioned(
            bottom: 2.h,
            left: 4.w,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Desk d8.',
                    style: kSemiBoldTextStyle.copyWith(
                        fontSize: 18.sp, color: ThemesApp.kWhiteColor)),
                SizedBox(width: 2.w),
                Text(unitModel.max.toString(),
                    style: kRegularTextStyle.copyWith(
                        fontSize: 16.sp, color: ThemesApp.kWhiteColor)),
                SizedBox(width: 1.w),
                SvgPicture.asset(
                  'assets/svgs/white-border-person.svg',
                  width: 4.w,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 2.h,
            right: 4.w,
            child: Text(
              '${'S.P'.tr} $price/${'hr'.tr}',
              style: kSemiBoldTextStyle.copyWith(
                  fontSize: 18.sp, color: ThemesApp.kWhiteColor),
            ),
          ),
        ],
      ),
    ),
  );
}
