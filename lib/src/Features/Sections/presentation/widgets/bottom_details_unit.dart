import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_gradient_button.dart';
import 'package:stories/src/Shared/presentation/widgets/from_to_capacity_widget.dart';

class BottomDetailsUnit extends StatelessWidget {
  const BottomDetailsUnit({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 2.w, right: 2.w),
      child: Container(
        height: 25.h,
        decoration: BoxDecoration(
            color: ThemesApp.kWhiteColor,
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(74, 74, 74, 0.12),
                offset: Offset(0, -12),
                blurRadius: 12,
              ),
            ],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12.sp),
                topRight: Radius.circular(12.sp))),
        child: Padding(
          padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 2.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const FromToCapacityWidget(
                  from: 'mon,may 15, 15:30',
                  to: 'mon,may 15, 15:30',
                  capacity: '5',
                  withColor: false),
              CustomGradientButton(
                title: 'SELECT THIS DESK'.tr,
                onPress: () {
                  Get.toNamed(AppRouter.kConfirmReserveScreen);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
