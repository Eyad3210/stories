import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_cubit.dart';
import 'package:stories/src/Features/Sections/presentation/widgets/categories_list.dart';
import 'package:stories/src/Features/Sections/presentation/widgets/home_appbar.dart';
import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../injector.dart';
import '../../../Auth/presentation/views/profile_screen.dart';
import '../../../Booking/presentation/views/my_booking_screen.dart';

class SectionScreenBody extends StatefulWidget {
  const SectionScreenBody({super.key});

  @override
  State<SectionScreenBody> createState() => _SectionScreenBodyState();
}

class _SectionScreenBodyState extends State<SectionScreenBody> {
  int _currentIndex = 0;
  List<Widget> pages = [
    BlocProvider<SectionsCubit>(
      create: (context) => injector(),
      child: const CategoriesList(),
    ),
    const MyBookingScreen(),
    BlocProvider<AuthCubit>(
      create: (context) => injector(),
      child: const ProfileScreen(),
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar:  const HomeAppBar(),
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: ThemesApp.kLightGrayColor.withOpacity(.5),
                offset: const Offset(0, -3),
                blurRadius: 12,
              ),
            ],
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(24),
              topRight: Radius.circular(24),
            ),
            child: SalomonBottomBar(
              selectedColorOpacity: 1,
              margin: const EdgeInsets.all(10),
              backgroundColor: Colors.white,
              currentIndex: _currentIndex,
              onTap: (i) => setState(() => _currentIndex = i),
              items: [
                SalomonBottomBarItem(
                  icon: SvgPicture.asset(
                    "assets/svgs/sections.svg",
                    color: _currentIndex == 0
                        ? Colors.white
                        : const Color(0xffa4a4a4),
                  ),
                  title: Text(
                    "Sections",
                    style: kMediumTextStyle.copyWith(
                        fontSize: 20, color: ThemesApp.kWhiteColor),
                  ),
                  selectedColor: ThemesApp.kPurpleColor,
                ),
                SalomonBottomBarItem(
                  icon: SvgPicture.asset(
                    "assets/svgs/orders.svg",
                    color: _currentIndex == 1
                        ? Colors.white
                        : const Color(0xffa4a4a4),
                  ),
                  title: Text(
                    "Bookings",
                    style: kMediumTextStyle.copyWith(
                        fontSize: 20, color: ThemesApp.kWhiteColor),
                  ),
                  selectedColor: ThemesApp.kPurpleColor,
                ),
                SalomonBottomBarItem(
                  icon: SvgPicture.asset(
                    "assets/svgs/user.svg",
                    color: _currentIndex == 2
                        ? Colors.white
                        : const Color(0xffa4a4a4),
                  ),
                  title: Text(
                    "Profile",
                    style: kMediumTextStyle.copyWith(
                        fontSize: 20, color: ThemesApp.kWhiteColor),
                  ),
                  selectedColor: ThemesApp.kPurpleColor,
                ),
              ],
            ),
            
            // BottomNavyBar(
            //   selectedIndex: _currentIndex,
            //   showElevation: true,
            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //   itemCornerRadius: 4.h,
            //   iconSize: 4.w,
            //   containerHeight: 8.h,
            //   curve: Curves.easeIn,
            //   onItemSelected: (index) => setState(() => _currentIndex = index),
            //   items: <BottomNavyBarItem>[
            //     BottomNavyBarItem(
            //       icon: SvgPicture.asset(
            //         'assets/svgs/sections.svg',
            //         color: _currentIndex == 0
            //             ? Colors.white
            //             : const Color(0xffa4a4a4),
            //       ),
            //       title: Text(
            //         'Sections',
            //         style: kMediumTextStyle.copyWith(
            //             fontSize: 16.sp, color: ThemesApp.kWhiteColor),
            //       ),
            //       activeColor: ThemesApp.kPurpleColor,
            //       textAlign: TextAlign.center,
            //     ),
            //     BottomNavyBarItem(
            //       icon: SvgPicture.asset(
            //         'assets/svgs/orders.svg',
            //         color: _currentIndex == 1
            //             ? Colors.white
            //             : const Color(0xffa4a4a4),
            //       ),
            //       title: Text(
            //         'My Booking',
            //         style: kMediumTextStyle.copyWith(
            //             fontSize: 16.sp, color: ThemesApp.kWhiteColor),
            //       ),
            //       activeColor: ThemesApp.kPurpleColor,
            //       textAlign: TextAlign.center,
            //     ),
            //     BottomNavyBarItem(
            //       icon: SvgPicture.asset(
            //         'assets/svgs/user.svg',
            //         color: _currentIndex == 2
            //             ? Colors.white
            //             : const Color(0xffa4a4a4),
            //       ),
            //       title: Text(
            //         'Profile',
            //         style: kMediumTextStyle.copyWith(
            //             fontSize: 16.sp, color: ThemesApp.kWhiteColor),
            //       ),
            //       activeColor: ThemesApp.kPurpleColor,
            //       textAlign: TextAlign.center,
            //     ),
            //   ],
            // ),
          
          
          ),
        ),
        body: Column(
          children: [
          //  const HomeAppBar(),
            Expanded(child: pages[_currentIndex]),
          ],
        ),
      ),
    );
  }
}
