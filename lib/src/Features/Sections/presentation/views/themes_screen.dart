import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/data/models/section_model.dart';
import 'package:stories/src/Features/Sections/presentation/views/units_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_app_bar.dart';

import '../widgets/custom_bottombar.dart';
import '../widgets/main_appbar.dart';
import '../widgets/sheet_reserve.dart';

class ThemesArguments {
  final List<SectionImage> sectionImage;
  final int price;
  final String themeName;

  ThemesArguments(this.sectionImage, this.price, this.themeName);
}

// ignore: must_be_immutable
class ThemesScreenBody extends StatefulWidget {
  const ThemesScreenBody({super.key, required this.themesArguments});

  final ThemesArguments themesArguments;
  @override
  State<ThemesScreenBody> createState() => _ThemesScreenBodyState();
}

class _ThemesScreenBodyState extends State<ThemesScreenBody> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: CustomBottomBar(
          title: "RESERVE NOW".tr,
          ontap: () {
            reserveBottomSheet(context, widget.themesArguments.price,
                widget.themesArguments.themeName);
          },
        ),
        backgroundColor: ThemesApp.kWhiteColor,
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(title: 'Themes'.tr),
            ),
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) => GridView.builder(
                  padding: EdgeInsets.all(1.h),
                  itemCount: widget.themesArguments.sectionImage.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: constraints.biggest.aspectRatio * 2 / 1.9,
                    mainAxisSpacing: 18,
                    crossAxisSpacing: 15,
                  ),
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      Get.toNamed(
                        AppRouter.kUnitScreen,
                        arguments: UnitsArguments(widget.themesArguments.price,
                            widget.themesArguments.themeName),
                      );
                    },
                    child: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                    widget.themesArguments.sectionImage[index]
                                        .image,
                                  ),
                                  fit: BoxFit.fill),
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(12.sp)),
                          width: constraints.maxWidth,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.sp),
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomCenter,
                              colors: colorList[index],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<String> images = [
  'assets/png/theme1.png',
  'assets/png/theme2.png',
  'assets/png/theme3.png',
  'assets/png/theme4.png'
];
List<List<Color>> colorList = [
  [
    const Color(0xffCD7672).withOpacity(0.5),
    const Color(0xffF85353).withOpacity(0.5),
  ],
  [
    const Color(0xff554768).withOpacity(0.5),
    const Color(0xff178186).withOpacity(0.5),
  ],
  [
    const Color(0xff178186).withOpacity(0.5),
    const Color(0xffCD7672).withOpacity(0.5),
  ],
  [
    const Color(0xffDC8665).withOpacity(0.5),
    const Color(0xffEEB461).withOpacity(0.5),
  ],
];
