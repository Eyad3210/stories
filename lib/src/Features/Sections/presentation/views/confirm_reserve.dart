import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../widgets/custom_bottombar.dart';

class ConfirmReserveScreen extends StatefulWidget {
  const ConfirmReserveScreen({super.key});

  @override
  State<ConfirmReserveScreen> createState() => _ConfirmReserveScreenState();
}

class _ConfirmReserveScreenState extends State<ConfirmReserveScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: CustomBottomBar(
          title: "CONFIRM".tr,
          ontap: () {},
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
                child: CustomAppBar(title: 'Confirm Reservation'.tr),
              ),
             GrayWidget(
                title: 'Confirm Reservation'.tr,
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "User name".tr,
                      style: kRegularTextStyle.copyWith(fontSize: 15.sp),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const CustomField(data: 'Bishr Alkhoja',),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      "Mobile number".tr,
                      style: kRegularTextStyle.copyWith(fontSize: 15.sp),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const CustomField(data: '0992889485',)
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
             GrayWidget(
                title: 'Reservation details'.tr,
              ),
              SizedBox(
                height: 30.h,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                           Expanded(
                              flex: 1,
                              child: StartOrEndContainer(
                                title: 'Start Time'.tr,
                                date: "Mon,May 15 ,10:30",
                                color: ThemesApp.kMintColor,
                                icon: 'assets/svgs/calender-mint.svg',
                              ),
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                           Expanded(
                              flex: 1,
                              child: StartOrEndContainer(
                                title: 'End Time'.tr,
                                date: "Mon,May 15 ,10:30",
                                color: ThemesApp.kPinkColor,
                                icon: 'assets/svgs/calender-red.svg',
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 1.h,
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: .5.w,
                                    color: ThemesApp.kPurpleColor,
                                  ),
                                  borderRadius: BorderRadius.circular(16.sp),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/svgs/borderperson.svg',
                                      width: 7.w,
                                      color: ThemesApp.kPurpleColor,
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Text(
                                          "4000 ${"Persons".tr}",
                                          style: kMediumTextStyle.copyWith(
                                            fontSize: 20.sp,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: .5.w,
                                    color: ThemesApp.kLightYellowColor,
                                  ),
                                  borderRadius: BorderRadius.circular(16.sp),
                                ),
                                child: Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 2.w),
                                  child: Center(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(
                                            'assets/svgs/coin.svg',
                                            width: 7.w,
                                          ),
                                          Text(
                                            "${"S.P".tr} 2500/${"hr".tr}",
                                            style: kMediumTextStyle.copyWith(
                                                fontSize: 20.sp),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class StartOrEndContainer extends StatelessWidget {
  final String title;
  final String date;
  final String icon;
  final Color color;
  const StartOrEndContainer({
    super.key,
    required this.title,
    required this.date,
    required this.icon,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: .5.w,
          color: color,
        ),
        borderRadius: BorderRadius.circular(16.sp),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon,
                width: 7.w,
              ),
              SizedBox(
                width: 2.w,
              ),
              Text(
                title,
                style: kRegularTextStyle.copyWith(fontSize: 20.sp),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.w),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Text(
                date,
                style: kMediumTextStyle.copyWith(fontSize: 15.sp),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class GrayWidget extends StatelessWidget {
  final String title;

  const GrayWidget({
    super.key,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 4.w,
      ),
      alignment: Alignment.center,
      height: 6.h,
      color: ThemesApp.kLightGrayColor.withOpacity(.2),
      child: Row(
        children: [
          Text(
            title,
            style: kSemiBoldTextStyle.copyWith(
                fontSize: 16.sp, color: ThemesApp.kBlackColor),
          ),
        ],
      ),
    );
  }
}

class CustomField extends StatelessWidget {
  final String data;

  const CustomField({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(1.h),
        border: Border.all(width: .1.w),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.2.h),
        child: Text(
          data,
          style: kMediumTextStyle.copyWith(
              fontSize: 15.sp, color: ThemesApp.kBlackColor),
        ),
      ),
    );
  }
}
