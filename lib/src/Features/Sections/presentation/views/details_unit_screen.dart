import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Booking/presentation/widgets/my_booking_details_widget.dart';
import 'package:stories/src/Features/Booking/presentation/widgets/my_booking_title_widget.dart';
import 'package:stories/src/Features/Sections/presentation/widgets/bottom_details_unit.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../data/models/unit_model.dart';

class DetailsUnitArguments {
  final UnitModel unitModel;
  final int price;
  final String themeName;

  DetailsUnitArguments(this.unitModel, this.price, this.themeName);
}

class DetailsUnitScreen extends StatefulWidget {
  const DetailsUnitScreen({super.key, required this.detailsUnitArguments});
  final DetailsUnitArguments detailsUnitArguments;

  @override
  State<DetailsUnitScreen> createState() => _DetailsUnitScreenState();
}

class _DetailsUnitScreenState extends State<DetailsUnitScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: const BottomDetailsUnit(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
                child: const CustomAppBar(title: 'Desk b8'),
              ),
              Image.network(
                widget.detailsUnitArguments.unitModel.image,
                height: 25.h,
                fit: BoxFit.cover,
                width: double.infinity,
              ),
              MyBookingTitleWidget(
                  unitId: 'Desk b8',
                  themeName: widget.detailsUnitArguments.themeName),
              const MyBookingDetailsWidget(capacity: '4', price: '3000'),
            ],
          ),
        ),
      ),
    );
  }
}
