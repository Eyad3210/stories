import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/data/models/unit_model.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_cubit.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_state.dart';
import 'package:stories/src/Features/Sections/presentation/views/details_unit_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/presentation/widgets/from_to_capacity_widget.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../widgets/unit_card.dart';

class UnitsSearchArguments {
  final int price;
  final String themeName;

  UnitsSearchArguments(
    this.price,
    this.themeName,
  );
}

class UnitScreenSeacrhBody extends StatefulWidget {
  const UnitScreenSeacrhBody({super.key, required this.unitsSearchArguments});
  // ignore: non_constant_identifier_names
  final UnitsSearchArguments unitsSearchArguments;
  @override
  State<UnitScreenSeacrhBody> createState() => _UnitScreenSeacrhBodyState();
}

class _UnitScreenSeacrhBodyState extends State<UnitScreenSeacrhBody> {
  final scrollController = ScrollController();

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<SectionsCubit>(context).loadUnits(sectionId: 1);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<SectionsCubit>(context).loadUnits(sectionId: 1);
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(title: widget.unitsSearchArguments.themeName),
            ),
            Expanded(
              child: BlocBuilder<SectionsCubit, SectionsState>(
                builder: (context, state) {
                  if (state is UnitLoading && state.isFirstFetch) {
                    return const Center(child: CircularProgressIndicator());
                  }

                  List<UnitModel> units = [];
                  bool isLoading = false;

                  if (state is UnitLoading) {
                    units = state.oldUnit;
                    isLoading = true;
                  } else if (state is UnitSuccess) {
                    units = state.listUnit;
                  }
                  return Column(
                    children: [
                      SizedBox(
                        height: 2.h,
                      ),
                      Padding(
                        padding:  EdgeInsets.only(bottom: 1.h),
                        child: const FromToCapacityWidget(
                          from: 'mon,may 15, 15:30',
                          to: 'mon,may 15, 15:30',
                          capacity: '4',
                          withColor: true,
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          controller: scrollController,
                          shrinkWrap: true,
                          itemCount: units.length + (isLoading ? 1 : 0),
                          itemBuilder: (context, index) {
                            if (index < units.length) {
                              return InkWell(
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  onTap: () {
                                    Get.toNamed(AppRouter.kDetailsUnitScreen,
                                        arguments: DetailsUnitArguments(
                                            units[index],
                                            widget.unitsSearchArguments.price,
                                            widget.unitsSearchArguments
                                                .themeName));
                                  },
                                  child: unitsWidget(units[index], context,
                                      widget.unitsSearchArguments.price));
                            } else {
                              Timer(const Duration(milliseconds: 30), () {
                                scrollController.jumpTo(
                                    scrollController.position.maxScrollExtent);
                              });

                              return const Center(
                                child: CircularProgressIndicator(
                                  color: ThemesApp.kPurpleColor,
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
