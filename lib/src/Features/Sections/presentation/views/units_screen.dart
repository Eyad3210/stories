import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Sections/data/models/unit_model.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_cubit.dart';
import 'package:stories/src/Features/Sections/presentation/blocs/sections_state.dart';
import 'package:stories/src/Features/Sections/presentation/views/details_unit_screen.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_app_bar.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../widgets/custom_bottombar.dart';
import '../widgets/sheet_reserve.dart';
import '../widgets/unit_card.dart';

class UnitsArguments {
  final int price;
  final String themename;

  UnitsArguments(this.price, this.themename);
}

class UnitScreenBody extends StatefulWidget {
  const UnitScreenBody({super.key, required this.unitsArguments});
  // ignore: non_constant_identifier_names
  final UnitsArguments unitsArguments;
  @override
  State<UnitScreenBody> createState() => _UnitScreenBodyState();
}

class _UnitScreenBodyState extends State<UnitScreenBody> {
  final scrollController = ScrollController();

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<SectionsCubit>(context).loadUnits(sectionId: 1);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<SectionsCubit>(context).loadUnits(sectionId: 1);
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        bottomNavigationBar: CustomBottomBar(
          title: "RESERVE NOW".tr,
          ontap: () {
            reserveBottomSheet(context, widget.unitsArguments.price,
                widget.unitsArguments.themename);
          },
        ),
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
              child: CustomAppBar(
                title: widget.unitsArguments.themename,
              ),
            ),
            Expanded(
              child: BlocBuilder<SectionsCubit, SectionsState>(
                builder: (context, state) {
                  if (state is UnitLoading && state.isFirstFetch) {
                    return const Center(child: CircularProgressIndicator());
                  }

                  List<UnitModel> units = [];
                  bool isLoading = false;

                  if (state is UnitLoading) {
                    units = state.oldUnit;
                    isLoading = true;
                  } else if (state is UnitSuccess) {
                    units = state.listUnit;
                  }
                  return Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          controller: scrollController,
                          shrinkWrap: true,
                          itemCount: units.length + (isLoading ? 1 : 0),
                          itemBuilder: (context, index) {
                            if (index < units.length) {
                              return InkWell(
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  onTap: () {
                                    Get.toNamed(AppRouter.kDetailsUnitScreen,
                                        arguments: DetailsUnitArguments(
                                            units[index],
                                            widget.unitsArguments.price,
                                            widget.unitsArguments.themename));
                                  },
                                  child: unitsWidget(units[index], context,
                                      widget.unitsArguments.price));
                            } else {
                              Timer(
                                const Duration(milliseconds: 30),
                                () {
                                  scrollController.jumpTo(scrollController
                                      .position.maxScrollExtent);
                                },
                              );

                              return const Center(
                                child: CircularProgressIndicator(
                                  color: ThemesApp.kPurpleColor,
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
