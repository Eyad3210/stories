class GetSectionsModel {
  final List<SectionModel> sections;
  GetSectionsModel({required this.sections});
/* factory GetSectionsModel.fromJson(Map<String, dynamic> json) => GetSectionsModel(
      
        sections: List<SectionModel>.from(
            json["data"].map((x) => SectionModel.fromJson(x))),
      );  */
  factory GetSectionsModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"] as List;
    return GetSectionsModel(
      sections: List<SectionModel>.from(
          dataList.map((x) => SectionModel.fromJson(x))),
    );
  }
}

class SectionModel {
  int id;
  String title;
  String body;
  String type;
  String description;
  int hourCost;
  String image;
  String? themeColor;
  int centerId;
  List<SectionImage> sectionImages;

  SectionModel({
    required this.id,
    required this.title,
    required this.body,
    required this.type,
    required this.description,
    required this.hourCost,
    required this.image,
    this.themeColor,
    required this.centerId,
    required this.sectionImages,
  });

  factory SectionModel.fromJson(Map<String, dynamic> json) => SectionModel(
        id: json["id"],
        title: json["title"],
        body: json["body"],
        type: json["type"],
        description: json["description"],
        hourCost: json["hour_cost"],
        image: json["image"],
        themeColor: json["theme_color"],
        centerId: json["center_id"],
        sectionImages: List<SectionImage>.from(
            json["section_images"].map((x) => SectionImage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "body": body,
        "type":type,
        "description": description,
        "hour_cost": hourCost,
        "image": image,
        "theme_color": themeColor,
        "center_id": centerId,
        "section_images":
            List<dynamic>.from(sectionImages.map((x) => x.toJson())),
      };
}

class SectionImage {
  int id;
  String image;
  int sectionId;

  SectionImage({
    required this.id,
    required this.image,
    required this.sectionId,
  });

  factory SectionImage.fromJson(Map<String, dynamic> json) => SectionImage(
        id: json["id"],
        image: json["image"],
        sectionId: json["section_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "section_id": sectionId,
      };
}
