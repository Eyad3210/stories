class GetUnitModel {
  
  final List<UnitModel> units;
  GetUnitModel({required this.units});

  factory GetUnitModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"]["data"] as List;
    return GetUnitModel(
      units: List<UnitModel>.from(
          dataList.map((x) => UnitModel.fromJson(x))),


    );
  }
}

class UnitModel {
    int id;
    int min;
    int max;
    int visible;
    String image;
    int sectionId;

    UnitModel({
        required this.id,
        required this.min,
        required this.max,
        required this.visible,
        required this.image,
        required this.sectionId,
    });

    factory UnitModel.fromJson(Map<String, dynamic> json) => UnitModel(
        id: json["id"],
        min: json["min"],
        max: json["max"],
        visible: json["visible"],
        image: json["image"],
        sectionId: json["section_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "min": min,
        "max": max,
        "visible": visible,
        "image": image,
        "section_id": sectionId,
    };
}