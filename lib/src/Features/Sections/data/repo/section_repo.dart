
import 'package:stories/src/Features/Sections/data/models/section_model.dart';
import 'package:stories/src/Features/Sections/data/models/unit_model.dart';

import '../../../../Shared/core/resources/data_state.dart';

abstract class SectionRepo{
  Future<DataState<GetSectionsModel>> getSections({required int centerId});


  Future<DataState<GetUnitModel>> getUnits({required int sectionId,required int page});

}