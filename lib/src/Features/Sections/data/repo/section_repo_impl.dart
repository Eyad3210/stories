import 'package:stories/src/Features/Sections/data/models/unit_model.dart';
import 'package:stories/src/Features/Sections/data/models/section_model.dart';
import 'package:stories/src/Features/Sections/data/repo/section_repo.dart';
import 'package:stories/src/Shared/core/resources/data_state.dart';

import '../../../../Shared/core/resources/data_repo_impl.dart';
import '../../../../Shared/core/resources/data_service.dart';

class SectionRepoImpl implements SectionRepo {
  // ignore: unused_field
  final DataService _dataService;
  // ignore: unused_field
  final DataRepoImpl _dataRepoImpl;

  SectionRepoImpl(this._dataService, this._dataRepoImpl);
  @override
  Future<DataState<GetSectionsModel>> getSections({required int centerId}) async {
    final response = await _dataService.get(
        endPoint: 'centers/1/sections', withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetSectionsModel.fromJson);
  }

  

  @override
  Future<DataState<GetUnitModel>> getUnits(
      {required int sectionId, required int page})async {
   final response = await _dataService.get(
        endPoint: 'sections/1/units?page=$page', withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetUnitModel.fromJson);
  }
}
