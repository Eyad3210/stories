import '../../../../Shared/core/resources/data_state.dart';
import '../models/my_hourly_package_model.dart';


abstract class PackageRepo{
  Future<DataState<GetMyHourlyPackageModel>> getMyHourlyPackages({required int page});
}