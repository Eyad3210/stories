import 'package:stories/src/Features/Packages/data/repo/package_repo.dart';
import 'package:stories/src/Shared/core/resources/data_state.dart';

import '../../../../Shared/core/resources/data_repo_impl.dart';
import '../../../../Shared/core/resources/data_service.dart';
import '../models/my_hourly_package_model.dart';

class PackageRepoImpl implements PackageRepo{
  final DataService _dataService;
  final DataRepoImpl _dataRepoImpl;

  PackageRepoImpl(this._dataService, this._dataRepoImpl);

  @override
  Future<DataState<GetMyHourlyPackageModel>> getMyHourlyPackages({required int page}) async{
    final response =
        await _dataService.get(endPoint: 'mine/hoursPackages?page=$page', withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetMyHourlyPackageModel.fromJson);
  }

}