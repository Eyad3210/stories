import 'package:equatable/equatable.dart';

class HourlyPackageModel extends Equatable {
  final int id;
  final String title;
  final int price;
  final int hours;
  final int validTo;
  final String image;
  final int centerId;
  final String? deletedAt;
  final String? createdAt;
  final String? updatedAt;

  const HourlyPackageModel({
    required this.id,
    required this.title,
    required this.price,
    required this.hours,
    required this.validTo,
    required this.image,
    required this.centerId,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  factory HourlyPackageModel.fromJson(Map<String, dynamic> json) =>
      HourlyPackageModel(
        id: json["id"],
        title: json["title"],
        price: json["price"],
        hours: json["hours"],
        validTo: json["valid_to"],
        image: json["image"],
        centerId: json["center_id"],
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  @override
  List<Object?> get props =>
      [
        id,
        title,
        price,
        hours,
        validTo,
        image,
        centerId,
        deletedAt,
        createdAt,
        updatedAt,
      ];
}
