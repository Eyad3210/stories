import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Packages/data/models/hourly_package_model.dart';

class GetMyHourlyPackageModel {
  final List<MyHourlyPackageModel> packages;

  GetMyHourlyPackageModel({
    required this.packages,
  });

  factory GetMyHourlyPackageModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"]["data"] as List;
    return GetMyHourlyPackageModel(
      packages: List<MyHourlyPackageModel>.from(
          dataList.map((x) => MyHourlyPackageModel.fromJson(x))),
    );
  }
}

class MyHourlyPackageModel extends Equatable {
  final int id;
  final String validTo;
  final int hours;
  final int? paymentId;
  final int userId;
  final int hoursPackageId;
  final String createdAt;
  final String updatedAt;
  final HourlyPackageModel hoursPackage;

  const MyHourlyPackageModel({
    required this.id,
    required this.validTo,
    required this.hours,
    this.paymentId,
    required this.userId,
    required this.hoursPackageId,
    required this.createdAt,
    required this.updatedAt,
    required this.hoursPackage,
  });

  factory MyHourlyPackageModel.fromJson(Map<String, dynamic> json) =>
      MyHourlyPackageModel(
        id: json["id"],
        validTo: json["valid_to"],
        hours: json["hours"],
        paymentId: json["payment_id"],
        userId: json["user_id"],
        hoursPackageId: json["hours_package_id"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        hoursPackage: HourlyPackageModel.fromJson(json["hours_package"]),
      );

  @override
  List<Object?> get props =>
      [
        id,
        validTo,
        hours,
        paymentId,
        userId,
        hoursPackageId,
        createdAt,
        updatedAt,
        hoursPackage,
      ];
}
