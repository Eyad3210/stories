import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Packages/presentation/blocs/package_cubit.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../../../Shared/presentation/widgets/custom_button.dart';
import '../widgets/package_container.dart';
import '../widgets/package_details_container.dart';

class MyHourlyPackagesScreen extends StatefulWidget {
  const MyHourlyPackagesScreen({Key? key}) : super(key: key);

  @override
  State<MyHourlyPackagesScreen> createState() => _MyHourlyPackagesScreenState();
}

class _MyHourlyPackagesScreenState extends State<MyHourlyPackagesScreen> {
  final PageController _pageController = PageController(initialPage: 0);

  int x = 0;

  List<String> list = ['200', '40', '120', '5'];

  int _currentPage = 0;

  Future<void> getMyHourlyPackages() async {
    await BlocProvider.of<PackageCubit>(context).getMyHourlyPackages();
  }

  void _onScrollListener(
      BuildContext context, ScrollController scrollController) {
    final maxScroll = scrollController.position.maxScrollExtent;
    final currentScroll = scrollController.position.pixels;

    if (maxScroll == currentScroll) {
      print('END');
      getMyHourlyPackages();
    }
  }

  @override
  void initState() {
    super.initState();
    getMyHourlyPackages();
    _pageController.addListener(() {
      _onScrollListener(context, _pageController);
      setState(() {
        _currentPage = _pageController.page!.round();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: 2.h,
              left: 4.w,
              right: 4.w,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CustomAppBar(
                  title: 'My hourly packages'.tr,
                ),
                BlocBuilder<PackageCubit, PackageState>(
                  builder: (context, state) {
                    if (state is GetMyHourlyPackagesLoading){
                      return const Center(child: CircularProgressIndicator(),);
                    }
                    if (state is GetMyHourlyPackagesEmpty){
                      return const Center(child: Text('empty'),);
                    }
                    if (state is GetMyHourlyPackagesError){
                      return const Center(child: Text('error'),);
                    }
                    if (state is GetMyHourlyPackagesDone) {
                      return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 3.h,
                        ),
                        SizedBox(
                          height: 50.w,
                          child: Scrollbar(
                            controller: _pageController,
                            thumbVisibility: true,
                            child: PageView.builder(
                                controller: _pageController,
                                scrollDirection: Axis.horizontal,
                                itemCount: state.packages.length,
                                itemBuilder: (BuildContext context, int index) {
                                  if(!state.noMoreData && index == state.packages.length - 1){
                                    return Padding(
                                      padding: EdgeInsets.all(8.h),
                                      child: const CircularProgressIndicator(),
                                    );
                                  }
                                  return PackageContainer(
                                    title: state.packages[index].hoursPackage.title,
                                    location: state.packages[index].hoursPackage.centerId.toString(),
                                    hours: state.packages[index].hoursPackage.hours.toString(),
                                    validate: state.packages[index].hoursPackage.validTo.toString(),
                                  );
                                }),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        AnimatedSwitcher(
                          duration: const Duration(milliseconds: 500),
                          child: Text(
                            key: ValueKey(state.packages[_currentPage].hoursPackage.id),
                            state.packages[_currentPage].hoursPackage.title,
                            textAlign: TextAlign.center,
                            style: kBoldTextStyle.copyWith(
                              color: ThemesApp.kBlackColor,
                              fontSize: 24.sp,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            PackageDetailsContainer(
                              packageId: state.packages[_currentPage].hoursPackage.id,
                              color: ThemesApp.kMintColor,
                              title: 'Start Date'.tr,
                              icon: 'calender-mint.svg',
                              data: state.packages[_currentPage].createdAt,
                            ),
                            PackageDetailsContainer(
                              packageId: state.packages[_currentPage].hoursPackage.id,
                              color: ThemesApp.kOrangeColor,
                              title: 'End Date'.tr,
                              icon: 'calender-red.svg',
                              data: 'Sun, Nov 15',
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            PackageDetailsContainer(
                              packageId: state.packages[_currentPage].hoursPackage.id,
                              color: ThemesApp.kMintColor,
                              title: 'Remaining hours'.tr,
                              icon: 'timer-start.svg',
                              data: state.packages[_currentPage].hours.toString(),
                            ),
                            PackageDetailsContainer(
                              packageId: state.packages[_currentPage].hoursPackage.id,
                              color: ThemesApp.kOrangeColor,
                              title: 'Consumed hours'.tr,
                              icon: 'timer-pause.svg',
                              data: (state.packages[_currentPage].hoursPackage.hours - state.packages[_currentPage].hours).toString(),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 4.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                          child: CustomButton(
                            borderColor: ThemesApp.kOrangeColor,
                            textColor: ThemesApp.kBlackColor,
                            iconColor: ThemesApp.kOrangeColor,
                            icon: 'assets/svgs/ticket-star.svg',
                            title: 'VIEW STORIES HOURLY PACKAGES'.tr,
                            onPress: () {
                            }, backgroundColor: ThemesApp.kWhiteColor,
                          ),
                        ),
                      ],
                    );
                    }
                    return const SizedBox();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
