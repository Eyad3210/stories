import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class PackageContainer extends StatelessWidget {
  final String title;
  final String location;
  final String hours;
  final String validate;

  const PackageContainer({
    super.key, required this.title, required this.location, required this.hours, required this.validate,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: SizedBox(
            width: 80.w,
            height: 50.w,
          ),
        ),
        Positioned(
          left: 10.w,
          child: Center(
            child: Container(
              width: 70.w,
              height: 50.w,
              decoration: BoxDecoration(
                color: ThemesApp.kLightGrayColor.withOpacity(.09),
                borderRadius: BorderRadius.circular(6.h),
              ),
              child: Padding(
                padding: EdgeInsets.only(left: 20.w),
                child: Column(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 4.h,
                          child: VerticalDivider(
                            color: ThemesApp.kOrangeColor,
                            thickness: .7.w,
                          ),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            title,
                            style: kBoldTextStyle.copyWith(
                              color: ThemesApp.kLightYellowColor,
                              fontSize: 18.sp,
                            ),
                          ),
                        ),
                        Divider(
                          color: ThemesApp.kOrangeColor,
                          height: 1.w,
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            'Stories',
                            style: kRegularTextStyle.copyWith(
                              color: ThemesApp.kGrayColor,
                              fontSize: 15.sp,
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            location,
                            style: kRegularTextStyle.copyWith(
                              color: ThemesApp.kGrayColor,
                              fontSize: 15.sp,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20.w, right: 3.w),
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Text(
                              '$hours hours',
                              style: kBoldTextStyle.copyWith(
                                color: ThemesApp.kBlackColor,
                                fontSize: 15.sp,
                              ),
                            ),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Text(
                              '$validate ${'days validity'.tr}',
                              style:
                              kRegularTextStyle.copyWith(
                                color: ThemesApp.kBlackColor,
                                fontSize: 15.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                      child: VerticalDivider(
                        color: ThemesApp.kOrangeColor,
                        thickness: .7.w,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 12.w,
          left: 1.w,
          child: Container(
            width: 25.w,
            height: 25.w,
            decoration: BoxDecoration(
              color: ThemesApp.kWhiteColor,
              shape: BoxShape.circle,
            ),
          ),
        ),
      ],
    );
  }
}
