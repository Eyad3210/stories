import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class PackageDetailsContainer extends StatelessWidget {
  final Color color;
  final int packageId;
  final String title;
  final String icon;
  final String data;
  const PackageDetailsContainer({
    super.key, required this.packageId, required this.color, required this.title, required this.icon, required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 15.h,
      width: 45.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.h),
        border: Border.all(color: color, width: .3.w),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 1.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset('assets/svgs/$icon', width: 4.w, height: 4.h,),
                SizedBox(width: 2.w,),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kRegularTextStyle.copyWith(
                    color: ThemesApp.kBlackColor,
                    fontSize: 18.sp,
                  ),
                ),
              ],
            ),

            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 500),
                child: Text(
                  key: ValueKey(packageId),
                  data,
                  textAlign: TextAlign.center,
                  style: kRegularTextStyle.copyWith(
                    color: ThemesApp.kBlackColor,
                    fontSize: 18.sp,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
