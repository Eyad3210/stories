part of 'package_cubit.dart';

abstract class PackageState extends Equatable {
  const PackageState();
  @override
  List<Object> get props => [];
}

class PackageEmpty extends PackageState {
  const PackageEmpty();
}

//My hourly packages
class GetMyHourlyPackagesEmpty extends PackageState {
  const GetMyHourlyPackagesEmpty();
}

class GetMyHourlyPackagesLoading extends PackageState {
  const GetMyHourlyPackagesLoading();
}

class GetMyHourlyPackagesDone extends PackageState {
  final List<MyHourlyPackageModel> packages;
  final bool noMoreData;

  const GetMyHourlyPackagesDone(this.packages, this.noMoreData);

  @override
  List<Object> get props => [packages, noMoreData];
}

class GetMyHourlyPackagesError extends PackageState {
  final DioError error;

  const GetMyHourlyPackagesError(this.error);

  @override
  List<Object> get props => [error];
}
