import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Packages/data/models/my_hourly_package_model.dart';
import 'package:stories/src/Features/Packages/data/repo/package_repo.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../../Shared/core/resources/data_state.dart';

part 'package_state.dart';

class PackageCubit extends Cubit<PackageState> {
  final PackageRepo _packageRepo;
  int _page = 1;
  List<MyHourlyPackageModel> _packages = [];
  static const int _pageSize = 15;

  PackageCubit(this._packageRepo) : super(const PackageEmpty());

  Future<void> getMyHourlyPackages() async{
    if (_packages.isEmpty){
      emit(const GetMyHourlyPackagesLoading());
    }
    final dataState = await _packageRepo.getMyHourlyPackages(page: _page);
    if (dataState is DataSuccess && dataState.data!.packages.isNotEmpty){
      final packages = dataState.data!.packages;
      final noMoreData =packages.length < _pageSize;
      _packages.addAll(packages);
      emit(GetMyHourlyPackagesDone(_packages, noMoreData));
      _page++;
    }
    if (_packages.isEmpty){
      emit(const GetMyHourlyPackagesEmpty());
    }
    if (dataState is DataFailed){
      emit(GetMyHourlyPackagesError(dataState.error!));
    }

    // emit(const GetMyHourlyPackagesLoading());
    // final dataState = await _packageRepo.getMyHourlyPackages();
    // if (dataState is DataSuccess){
    //   if (dataState.data!.packages.isEmpty){
    //     emit(const GetMyHourlyPackagesEmpty());
    //   }
    //   else {
    //     emit(GetMyHourlyPackagesDone(dataState.data!));
    //   }
    // }
    // if (dataState is DataFailed){
    //   emit(GetMyHourlyPackagesError(dataState.error!));
    // }
  }

  @override
  void onChange(Change<PackageState> change) {
    super.onChange(change);
    print(change);
  }

}
