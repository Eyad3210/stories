import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_app_bar.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_button.dart';
import 'package:stories/src/Shared/presentation/widgets/from_to_capacity_widget.dart';

import '../widgets/my_booking_details_widget.dart';
import '../widgets/my_booking_title_widget.dart';
import '../widgets/qr_widget.dart';

class UpcomingBookingDetailsScreen extends StatelessWidget {
  const UpcomingBookingDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: 2.h,
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: const CustomAppBar(title: 'Booking No. 1321'),
                ),
                SizedBox(
                  height: 3.h,
                ),
                SizedBox(
                  height: 30.h,
                  child: PageView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 4,
                    itemBuilder: ((context, index) => Image.network(
                          'https://imgv3.fotor.com/images/blog-richtext-image/part-blurry-image.jpg',
                          height: 30.h,
                          fit: BoxFit.cover,
                          width: double.infinity,
                        )),
                  ),
                ),
                const MyBookingTitleWidget(
                  unitId: 'Desk d8',
                  themeName: 'nostaliga',
                ),
                const MyBookingDetailsWidget(capacity: "4", price: '2500',),
                SizedBox(
                  height: 2.h,
                ),
                const FromToCapacityWidget(
                    from: 'Mon, May 15, 10:30',
                    to: 'Mon, May 15, 10:30',
                    capacity: '4', withColor: false,),
                SizedBox(
                  height: 3.h,
                ),
                QrWidget(id: "bishr", text: 'Scan to start your session'.tr,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
                  child: CustomButton(
                      title: 'Cancel Your Booking'.tr,
                      icon: 'assets/svgs/close-circle.svg',
                      onPress: () {},
                      borderColor: ThemesApp.kRedColor,
                      textColor: ThemesApp.kBlackColor,
                      backgroundColor: ThemesApp.kWhiteColor),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}