import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../widgets/my_booking_card.dart';

class MyBookingScreen extends StatefulWidget {
  const MyBookingScreen({Key? key}) : super(key: key);

  @override
  State<MyBookingScreen> createState() => _MyBookingScreenState();
}

class _MyBookingScreenState extends State<MyBookingScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose(); // dispose of the controller when not needed
    super.dispose();
  }

  void _onTabChanged(int index) {
    setState(() {
      tabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 3.w),
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: DefaultTabController(
          length: 4,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Material(
                  color: ThemesApp.kWhiteColor,
                  child: TabBar(
                    onTap: _onTabChanged,
                    overlayColor:
                        MaterialStateProperty.all<Color>(Colors.transparent),
                    unselectedLabelColor: ThemesApp.kBlackColor,
                    unselectedLabelStyle: kMediumTextStyle.copyWith(
                      fontSize: 13.sp,
                    ),
                    labelColor: ThemesApp.kPurpleColor.withOpacity(.7),
                    labelStyle: kMediumTextStyle.copyWith(
                      fontSize: 13.sp,
                    ),
                    controller: _tabController,
                    indicatorWeight: .4.h,
                    indicatorColor: ThemesApp.kPurpleColor.withOpacity(.9),
                    tabs: [
                      Tab(
                        height: 5.h,
                        text: 'Upcoming'.tr,
                      ),
                      Tab(
                        height: 5.h,
                        text: 'Running Now'.tr,
                      ),
                      Tab(
                        height: 5.h,
                        text: 'Ended'.tr,
                      ),
                      Tab(
                        height: 5.h,
                        text: 'Cancelled'.tr,
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                  color: ThemesApp.kWhiteColor,
                    boxShadow: [
                      BoxShadow(
                        color: ThemesApp.kDarkGrayColor,
                        blurRadius: 1,
                        spreadRadius: .05.h,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: const SizedBox(
                    width: double.infinity,
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                tabIndex == 0
                    ? MyBookingCard(
                        id: 5374,
                        from: 'mon,may 15, 15:30',
                        to: 'mon,may 15, 15:30',
                        capacity: '4',
                        backgroundColor: ThemesApp.kPurpleColor,
                        onPress: () {
                          Get.toNamed(AppRouter.kUpcomingBookingDetailsScreen);
                        },
                      )
                    : tabIndex == 1
                        ? MyBookingCard(
                            id: 1231,
                            from: 'mon,may 15, 15:30',
                            to: 'mon,may 15, 15:30',
                            capacity: '2',
                            backgroundColor: ThemesApp.kMintColor,
                            onPress: () {
                              Get.toNamed(
                                  AppRouter.kRunningNowBookingDetailsScreen);
                            },
                          )
                        : const SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
