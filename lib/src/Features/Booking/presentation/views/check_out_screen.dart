import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/Shared/core/util/constants.dart';
import 'package:stories/src/Shared/presentation/widgets/from_to_capacity_widget.dart';

import '../../../../Shared/presentation/widgets/doted_line.dart';
import '../widgets/bill_details_row.dart';
import '../widgets/bill_payment_details_row.dart';
import '../widgets/payment_radio_button.dart';

class CheckOutScreen extends StatelessWidget {
  const CheckOutScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 1.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Checkout'.tr,
                  style: kBoldTextStyle.copyWith(
                    fontSize: 22.sp,
                  ),
                ),
                SizedBox(
                  height: 3.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Bill Details'.tr,
                          style: kBoldTextStyle.copyWith(
                            fontSize: 18.sp,
                          )),
                      const Text('Date: 15/05/2023 - 12:00'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: Row(
                    children: [
                      Text(
                        'Desk D8.',
                        style: kBoldTextStyle.copyWith(
                            fontSize: 22.sp, color: ThemesApp.kBlackColor),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                const FromToCapacityWidget(
                    from: 'Mon, May 15, 10:30',
                    to: 'Mon, May 15, 10:30',
                    capacity: '2', withColor: false,),
                SizedBox(
                  height: 2.h,
                ),
                Container(
                  width: double.infinity,
                  height: 6.h,
                  color: ThemesApp.kPinkColor,
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 48.w,
                        child: Text(
                          'Item'.tr,
                          style: kBoldTextStyle.copyWith(
                            fontSize: 20.sp,
                            color: ThemesApp.kWhiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.w,
                        child: Text(
                          'Price'.tr,
                          style: kBoldTextStyle.copyWith(
                            fontSize: 20.sp,
                            color: ThemesApp.kWhiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 11.w,
                        child: Text(
                          'Qty'.tr,
                          style: kBoldTextStyle.copyWith(
                            fontSize: 20.sp,
                            color: ThemesApp.kWhiteColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.w,
                        child: Text(
                          'Total'.tr,
                          style: kBoldTextStyle.copyWith(
                            fontSize: 20.sp,
                            color: ThemesApp.kWhiteColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const BillDetailsRow(
                  backgroundColor: ThemesApp.kWhiteColor,
                  item: 'Desk booking for 4 persons',
                  price: '2000',
                  qty: '5',
                  total: '20000',
                ),
                BillDetailsRow(
                  backgroundColor: ThemesApp.kLightGrayColor.withOpacity(.15),
                  item: 'Desk booking for 4 persons',
                  price: '2000',
                  qty: '5',
                  total: '20000',
                ),
                BillPaymentDetailsRow(
                  title: 'Subtotal'.tr,
                  value: '32000',
                  style: kRegularTextStyle.copyWith(
                    fontSize: 18.sp,
                    color: ThemesApp.kBlackColor,
                  ),
                ),
                DottedDash(
                  color: ThemesApp.kGrayColor.withOpacity(.3),
                ),
                BillPaymentDetailsRow(
                  title: 'Consumer spending fee 5%',
                  value: '1975',
                  style: kRegularTextStyle.copyWith(
                    fontSize: 15.sp,
                    color: ThemesApp.kBlackColor,
                  ),
                ),
                BillPaymentDetailsRow(
                  title: 'Local administration fee',
                  value: '25.5',
                  style: kRegularTextStyle.copyWith(
                    fontSize: 15.sp,
                    color: ThemesApp.kBlackColor,
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                DottedDash(
                  color: ThemesApp.kGrayColor.withOpacity(.3),
                ),
                BillPaymentDetailsRow(
                  title: 'Final Total'.tr,
                  value: '2000000',
                  color: ThemesApp.kLightGrayColor.withOpacity(.15),
                  style: kBoldTextStyle.copyWith(
                    fontSize: 18.sp,
                    color: ThemesApp.kBlackColor,
                  ),
                ),
                DottedDash(
                  color: ThemesApp.kGrayColor.withOpacity(.3),
                ),
                SizedBox(
                  height: 2.h,
                ),
                // Container(
                //   color: Colors.amber,
                //   height: 20.h,
                //   child: PaymentRadioButton(
                //     options: ['a', 'b'],
                //     onSelected: (String value) {
                //       print(value);
                //     },
                //     optionsIcons: [],
                //   ),
                // )
              
              ],
            ),
          ),
        ),
      ),
    );
  }
}

