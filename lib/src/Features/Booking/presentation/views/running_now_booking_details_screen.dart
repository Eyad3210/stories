import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Booking/presentation/widgets/qr_widget.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../../../Shared/presentation/widgets/custom_button.dart';
import '../../../../Shared/presentation/widgets/from_to_capacity_widget.dart';
import '../widgets/my_booking_details_widget.dart';
import '../widgets/my_booking_title_widget.dart';

class RunningNowBookingDetailsScreen extends StatelessWidget {
  const RunningNowBookingDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: 2.h,
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: const CustomAppBar(title: 'Booking No. 1321', withCart: true,),
                ),
                SizedBox(
                  height: 3.h,
                ),
                const MyBookingTitleWidget(
                  unitId: 'Desk d8',
                  themeName: 'nostaliga',
                ),
                const MyBookingDetailsWidget(
                  capacity: "4",
                  price: '2500',
                ),
                SizedBox(
                  height: 2.h,
                ),
                const FromToCapacityWidget(
                    from: 'Mon, May 15, 10:30',
                    to: 'Mon, May 15, 10:30',
                    capacity: '4', withColor: false,),
                SizedBox(
                  height: 3.h,
                ),
                InkWell(
                  overlayColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  onTap: () {
                    Get.toNamed(AppRouter.kCategoriesScreen);
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    child: Stack(
                      children: [
                        LayoutBuilder(
                          builder: (BuildContext context,
                              BoxConstraints constraints) {
                            return Container(
                              width: constraints.maxWidth,
                              height: constraints.maxWidth / 1.8,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2.h),
                                image: const DecorationImage(
                                  image: AssetImage(
                                    'assets/png/kitchen.png',
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                        ),
                        LayoutBuilder(
                          builder: (BuildContext context,
                              BoxConstraints constraints) {
                            return SizedBox(
                              width: constraints.maxWidth,
                              height: constraints.maxWidth / 1.8,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2.h),
                                  gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      ThemesApp.kPinkColor,
                                      Colors.white.withOpacity(0.01),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        LayoutBuilder(
                          builder: (BuildContext context,
                              BoxConstraints constraints) {
                            return SizedBox(
                              width: constraints.maxWidth,
                              height: constraints.maxWidth / 1.8,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2.h),
                                  gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: [
                                      ThemesApp.kPinkColor,
                                      ThemesApp.kWhiteColor.withOpacity(0.01)
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        Positioned(
                          left: 3.w,
                          top: 13.h,
                          child: SizedBox(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Kitchen Menu'.tr,
                                style: kBoldTextStyle.copyWith(
                                    fontSize: 30.sp,
                                    color: ThemesApp.kWhiteColor,
                                    height: .7),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Make your order now!'.tr,
                                    style: kRegularTextStyle.copyWith(
                                        fontSize: 15.sp,
                                        color: ThemesApp.kWhiteColor,
                                        height: .7),
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                  ),
                                  SvgPicture.asset(
                                    'assets/svgs/arrow-circle-right.svg',
                                    width: 2.w,
                                    height: 2.h,
                                  ),
                                ],
                              ),
                            ],
                          )),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 3.h,
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed(AppRouter.kCheckOutScreen);
                  },
                  child: QrWidget(
                    id: 'id',
                    text: 'Scan to end your session'.tr,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
                  child: CustomButton(
                      title: 'Extend Your Booking'.tr,
                      icon: 'assets/svgs/refresh.svg',
                      onPress: () {},
                      borderColor: ThemesApp.kLightYellowColor,
                      textColor: ThemesApp.kBlackColor,
                      backgroundColor: ThemesApp.kWhiteColor),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
