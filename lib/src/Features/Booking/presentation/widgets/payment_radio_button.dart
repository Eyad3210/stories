import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../Auth/presentation/widgets/radio_button.dart';

class PaymentRadioButton extends StatefulWidget {
  final List<String> options;
  final List<Widget> optionsIcons;
  final Function(String) onSelected;
  final int? initValue;

  const PaymentRadioButton(
      {super.key,
      required this.options,
      required this.optionsIcons,
      required this.onSelected,
      this.initValue});

  @override
  State<PaymentRadioButton> createState() => _PaymentRadioButtonState();
}

class _PaymentRadioButtonState extends State<PaymentRadioButton> {
  final List<int> _selectedOption = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.options.length; i++) {
      _selectedOption.add(-1);
    }
    if (widget.initValue != null) {
      _selectedOption[widget.initValue!] = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return InkWell(
          overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
          onTap: () {
            setState(() {
              for (int i = 0; i < _selectedOption.length; i++) {
                if (i != index) {
                  _selectedOption[i] = -1;
                } else {
                  _selectedOption[i] = 1;
                  widget.onSelected(widget.options.elementAt(i));
                }
              }
            });
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.h),
            child: Container(
              padding: EdgeInsets.all(5.w),
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(2.h),
              ),
              child: Row(
                children: [
                  Row(
                    children: [
                      RadioButton(
                        isSelected: _selectedOption.elementAt(index),
                        index: index,
                        color: const [
                          ThemesApp.kPurpleColor,
                          ThemesApp.kPurpleColor
                        ],
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text('data'),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
      itemCount: widget.options.length,
    );
  }
}
