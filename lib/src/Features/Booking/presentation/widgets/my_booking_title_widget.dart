import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class MyBookingTitleWidget extends StatelessWidget {
  final String unitId;
  final String themeName;

  const MyBookingTitleWidget({
    super.key,
    required this.unitId,
    required this.themeName,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              unitId,
              style: kBoldTextStyle.copyWith(
                  fontSize: 22.sp, color: ThemesApp.kBlackColor),
            ),
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.all(1.w),
                decoration: BoxDecoration(
                  color: ThemesApp.kPinkColor.withOpacity(0.29),
                  borderRadius: BorderRadius.circular(5.h),
                  border: Border.all(
                    color: ThemesApp.kPinkColor,
                    width: .1.h,
                  ),
                ),
                child: Center(
                  child: Text(
                    themeName,
                    style: kRegularTextStyle.copyWith(
                        fontSize: 16.sp, color: ThemesApp.kPinkColor),
                  ),
                ),
              ),
              SizedBox(
                width: 2.w,
              ),
              Container(
                padding: EdgeInsets.all(1.w),
                decoration: BoxDecoration(
                  color: ThemesApp.kGrayColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(5.h),
                ),
                child: SvgPicture.asset(
                  'assets/svgs/heart.svg',
                  height: 3.h,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
