import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

class BillPaymentDetailsRow extends StatelessWidget {
  final String title;
  final String value;
  final TextStyle style;
  final Color color;

  const BillPaymentDetailsRow({
    super.key,
    required this.title,
    required this.value,
    required this.style,
    this.color = ThemesApp.kWhiteColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 1.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: style),
            Row(
              children: [
                Text(
                  value,
                  style: style,
                ),
                SizedBox(
                  width: 5.w,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
