import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class QrWidget extends StatelessWidget {
  final String id;
  final String text;

  const QrWidget({
    super.key,
    required this.id, required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.h),
            border: Border.all(color: ThemesApp.kPinkColor, width: 1.w),
          ),
          child: Padding(
            padding: EdgeInsets.all(3.w),
            child: QrImageView(
              data: id,
              size: 45.w,
            ),
          ),
        ),
        SizedBox(
          height: 1.h,
        ),
        Text(
          text,
          style: kMediumTextStyle.copyWith(
              fontSize: 18.sp, fontStyle: FontStyle.italic),
        ),
      ],
    );
  }
}
