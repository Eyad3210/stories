import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_button.dart';
import '../../../../Shared/presentation/widgets/from_to_capacity_widget.dart';

class MyBookingCard extends StatelessWidget {
  final int id;
  final String from;
  final String to;
  final String capacity;
  final Color backgroundColor;
  final VoidCallback onPress;

  const MyBookingCard({
    super.key,
    required this.id,
    required this.from,
    required this.to,
    required this.capacity,
    required this.backgroundColor,
    required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.8.w, vertical: 3.w),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: ThemesApp.kWhiteColor,
          borderRadius: BorderRadius.circular(3.h),
          boxShadow: const [
            BoxShadow(
              color: ThemesApp.kDarkGrayColor,
              blurRadius: 4.0,
              spreadRadius: .1,
              offset: Offset(0, 0),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(2.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Booking No. $id',
                style: kBoldTextStyle.copyWith(fontSize: 18.sp),
              ),
              SizedBox(
                height: 2.h,
              ),
              FromToCapacityWidget(from: from, to: to, capacity: capacity, withColor: false,),
              SizedBox(
                height: 3.h,
              ),
              CustomButton(
                title: 'SHOW DETAILS'.tr,
                onPress: onPress,
                borderColor: backgroundColor,
                textColor: ThemesApp.kWhiteColor,
                backgroundColor: backgroundColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
