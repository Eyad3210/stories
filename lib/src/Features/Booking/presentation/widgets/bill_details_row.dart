import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class BillDetailsRow extends StatelessWidget {
  final String item;
  final String price;
  final String qty;
  final String total;
  final Color backgroundColor;

  const BillDetailsRow({
    super.key,
    required this.item,
    required this.price,
    required this.qty,
    required this.total,
    required this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 1.h),
        child: Row(
          children: [
            SizedBox(
              width: 48.w,
              child: Text(
                item,
                style: kRegularTextStyle.copyWith(
                  fontSize: 15.sp,
                  color: ThemesApp.kBlackColor,
                ),
              ),
            ),
            SizedBox(
              width: 15.w,
              child: Text(
                price,
                style: kRegularTextStyle.copyWith(
                  fontSize: 15.sp,
                  color: ThemesApp.kBlackColor,
                ),
              ),
            ),
            SizedBox(
              width: 11.w,
              child: Text(
                qty,
                style: kRegularTextStyle.copyWith(
                  fontSize: 15.sp,
                  color: ThemesApp.kBlackColor,
                ),
              ),
            ),
            SizedBox(
              width: 15.w,
              child: Text(
                total,
                style: kRegularTextStyle.copyWith(
                  fontSize: 15.sp,
                  color: ThemesApp.kBlackColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
