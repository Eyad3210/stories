import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class MyBookingDetailsWidget extends StatelessWidget {
  final String capacity;
  final String price;

  const MyBookingDetailsWidget({
    super.key,
    required this.capacity,
    required this.price,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
      decoration: BoxDecoration(
        color: ThemesApp.kLightGrayColor.withOpacity(.15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SvgPicture.asset(
                'assets/svgs/redperson.svg',
                height: 3.5.h,
              ),
              Text(
                "Capacity".tr,
                style: kMediumTextStyle.copyWith(
                    fontSize: 18.sp, color: ThemesApp.kBlackColor),
              ),
              SizedBox(
                width: 2.w,
              ),
              Text(
                capacity,
                style: kBoldTextStyle.copyWith(
                    fontSize: 18.sp, color: ThemesApp.kBlackColor),
              ),
              Text(
                " ${"Persons".tr}",
                style: kMediumTextStyle.copyWith(
                    fontSize: 18.sp, color: ThemesApp.kBlackColor),
              ),
            ],
          ),
          Expanded(
            child: Text(
              'S.P $price/hr',
              textAlign: TextAlign.end,
              style: kBoldTextStyle.copyWith(
                  fontSize: 20.sp, color: ThemesApp.kBlackColor),
            ),
          ),
          SizedBox(
            width: 1.w,
          ),
        ],
      ),
    );
  }
}
