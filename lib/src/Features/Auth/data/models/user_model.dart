import 'package:equatable/equatable.dart';

class GetUserModel {
  final UserModel data;

  GetUserModel({
    required this.data,
  });

  factory GetUserModel.fromJson(Map<String, dynamic> json) => GetUserModel(
    data: UserModel.fromJson(json["data"]),
  );
}

class UserModel extends Equatable {
  const UserModel({
    required this.id,
    required this.name,
    required this.accountType,
    required this.roleId,
    required this.phone,
    required this.phoneVerifiedAt,
    required this.email,
    required this.emailVerifiedAt,
    required this.gender,
    required this.work,
    required this.birthday,
    required this.banPoints,
  });

  final int id;
  final String name;
  final String accountType;
  final int roleId;
  final String phone;
  final dynamic phoneVerifiedAt;
  final String email;
  final dynamic emailVerifiedAt;
  final String gender;
  final String work;
  final String birthday;
  final int banPoints;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      UserModel(
        id: json["id"],
        name: json["name"],
        accountType: json["account_type"],
        roleId: json["role_id"],
        phone: json["phone"],
        phoneVerifiedAt: json["phone_verified_at"],
        email: json["email"],
        emailVerifiedAt: json["email_verified_at"],
        gender: json["gender"],
        work: json["work"],
        birthday: json["birthday"],
        banPoints: json["ban_points"],
      );

  Map<String, dynamic> toJson() =>
      {
        "id": id,
        "name": name,
        "account_type": accountType,
        "role_id": roleId,
        "phone": phone,
        "phone_verified_at": phoneVerifiedAt,
        "email": email,
        "email_verified_at": emailVerifiedAt,
        "gender": gender,
        "work": work,
        "birthday": birthday,
        "ban_points": banPoints,
      };

  @override
  List<Object> get props =>
      [
        id,
        name,
        accountType,
        roleId,
        phone,
        phoneVerifiedAt,
        email,
        emailVerifiedAt,
        gender,
        work,
        birthday,
        banPoints,
      ];
}