import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Auth/data/models/user_model.dart';

class GetSignModel {
  final SignModel data;

  GetSignModel({
    required this.data,
  });

  factory GetSignModel.fromJson(Map<String, dynamic> json) => GetSignModel(
    data: SignModel.fromJson(json["data"]),
  );
}

class SignModel extends Equatable {
  const SignModel({
    required this.user,
    required this.token,
  });

  final UserModel user;
  final String token;

  factory SignModel.fromJson(Map<String, dynamic> json) =>
      SignModel(
        token: json["token"],
        user: UserModel.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() =>
      {
        "user": user.toJson(),
        "token": token,
      };

  @override
  List<Object> get props => [user, token];
}
