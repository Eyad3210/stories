import 'package:equatable/equatable.dart';

class GetTokenModel {
  final TokenModel data;

  GetTokenModel({
    required this.data,
  });

  factory GetTokenModel.fromJson(Map<String, dynamic> json) => GetTokenModel(
    data: TokenModel.fromJson(json["data"]),
  );
}

class TokenModel extends Equatable {
  const TokenModel({
    required this.token,
  });

  final String token;

  factory TokenModel.fromJson(Map<String, dynamic> json) =>
      TokenModel(
        token: json["token"],
      );

  Map<String, dynamic> toJson() =>
      {
        "token": token,
      };

  @override
  List<Object> get props => [token];
}
