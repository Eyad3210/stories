import 'package:dio/dio.dart';
import 'package:stories/src/Features/Auth/core/params/send_code_params.dart';
import 'package:stories/src/Features/Auth/core/params/verify_account_params.dart';
import 'package:stories/src/Features/Auth/data/models/token_model.dart';
import '../../../../Shared/core/resources/data_repo_impl.dart';
import '../../../../Shared/core/resources/data_service.dart';
import '../../../../Shared/core/resources/data_state.dart';
import '../../../../Shared/data/models/message_model.dart';
import '../../../../Shared/data/models/string_data_model.dart';
import '../../core/params/edit_profile_params.dart';
import '../../core/params/sign_in_params.dart';
import '../../core/params/sign_up_params.dart';
import '../models/sign_model.dart';
import '../models/user_model.dart';
import 'auth_repo.dart';

class AuthRepoImpl implements AuthRepo {
  final DataService _dataService;
  final DataRepoImpl _dataRepoImpl;

  AuthRepoImpl(this._dataService, this._dataRepoImpl);

  @override
  Future<DataState<GetSignModel>> signUp({required SignUpParams params}) async {
    var formData = FormData.fromMap(params.toJson());
    final response = await _dataService.post(
        endPoint: 'register', formData: formData, withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetSignModel.fromJson);
  }

  @override
  Future<DataState<GetSignModel>> signIn({required SignInParams params}) async {
    var formData = FormData.fromMap(params.toJson());
    final response = await _dataService.post(
        endPoint: 'login', formData: formData, withToken: false);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetSignModel.fromJson);
  }

  @override
  Future<DataState<GetStringDataModel>> logOut() async {
    final response = await _dataService.post(
        endPoint: 'logout', withToken: true, formData: null);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetStringDataModel.fromJson);
  }

  @override
  Future<DataState<GetUserModel>> getProfile() async {
    final response =
        await _dataService.get(endPoint: 'profile', withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetUserModel.fromJson);
  }

  @override
  Future<DataState<GetUserModel>> editProfile(
      {required EditProfileParams params}) async {
    var formData = FormData.fromMap(params.toJson());
    final response = await _dataService.post(
        endPoint: 'edit', formData: formData, withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetUserModel.fromJson);
  }

  @override
  Future<DataState<GetMessageModel>> sendCode({required SendCodeParams params}) async{
    var formData = FormData.fromMap(params.toJson());
    final response = await _dataService.post(
        endPoint: 'notify', formData: formData, withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetMessageModel.fromJson);
  }

  @override
  Future<DataState<GetUserModel>> verifyAccount({required VerifyAccountParams params}) async{
    var formData = FormData.fromMap(params.toJson());
    final response = await _dataService.post(
        endPoint: 'verify', formData: formData, withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetUserModel.fromJson);
  }

  @override
  Future<DataState<GetTokenModel>> refreshToken() async {
    final response = await _dataService.get(endPoint: 'refresh', withToken: true);
    return _dataRepoImpl.dataRepoRequest(
        response: response, fromJson: GetTokenModel.fromJson);
  }
}
