import '../../../../Shared/core/resources/data_state.dart';
import '../../../../Shared/data/models/message_model.dart';
import '../../../../Shared/data/models/string_data_model.dart';
import '../../core/params/edit_profile_params.dart';
import '../../core/params/send_code_params.dart';
import '../../core/params/sign_in_params.dart';
import '../../core/params/sign_up_params.dart';
import '../../core/params/verify_account_params.dart';
import '../models/sign_model.dart';
import '../models/token_model.dart';
import '../models/user_model.dart';

abstract class AuthRepo{
  Future<DataState<GetSignModel>> signUp({required SignUpParams params});

  Future<DataState<GetSignModel>> signIn({required SignInParams params});

  Future<DataState<GetStringDataModel>> logOut();

  Future<DataState<GetTokenModel>> refreshToken();

  Future<DataState<GetMessageModel>> sendCode({required SendCodeParams params});

  Future<DataState<GetUserModel>> verifyAccount({required VerifyAccountParams params});

  Future<DataState<GetUserModel>> getProfile();

  Future<DataState<GetUserModel>> editProfile({required EditProfileParams params});
}