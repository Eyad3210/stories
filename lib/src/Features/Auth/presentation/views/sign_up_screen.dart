import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/core/args/sign_up_args.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/custom_read_only_text_field.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/gender_radio_button.dart';
import '../widgets/sign_back_button.dart';
import '../widgets/underline_pressed_text.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SignBackButton(),
              Padding(
                padding: EdgeInsets.only(top: 3.5.h),
                child: Center(
                    child: Hero(
                  tag: 'stories',
                  child: Image(
                    image: const AssetImage('assets/images/stories.png'),
                    width: 70.w,
                    height: 20.h,
                  ),
                )),
              ),
              Center(
                  child: Text(
                'Create Your Account!'.tr,
                style: kBoldTextStyle.copyWith(fontSize: 23.sp),
              )),
              Center(
                  child: Text(
                'Enter the required information below please'.tr,
                style: kRegularTextStyle.copyWith(fontSize: 13.sp),
              )),
              SizedBox(
                height: 2.h,
              ),
              const FormContainer(),
            ],
          ),
        ),
      ),
    );
  }
}

class FormContainer extends StatefulWidget {
  const FormContainer({
    super.key,
  });

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  String _nameHintText = 'Full Name'.tr;
  String _workHintText = 'Working Field'.tr;
  String _birthdayHintText = 'Birthday'.tr;
  String _gender = '';

  final TextEditingController _nameCon = TextEditingController();
  final TextEditingController _workCon = TextEditingController();

  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _workFocusNode = FocusNode();
  final FocusNode _birthdayFocusNode = FocusNode();

  bool _nameHasError = false;
  bool _workHasError = false;
  bool _genderHasError = false;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: ThemesApp.kDarkGrayColor,
              blurRadius: 10.0,
              spreadRadius: -7,
              offset: Offset(0, -10),
            ),
          ],
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(5.h), topLeft: Radius.circular(5.h))),
      child: Padding(
        padding: EdgeInsets.only(
          top: 5.h,
          right: 5.w,
          left: 5.w,
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomTextField(
                hasError: _nameHasError,
                validator: (val) {
                  if (_nameCon.text == '') {
                    setState(() {
                      _nameHasError = true;
                    });
                    return 'Full name required'.tr;
                  }
                  setState(() {
                    _nameHasError = false;
                  });
                  return null;
                },
                hintText: _nameHintText,
                labelText: 'Full Name'.tr,
                suffixIcon: SvgPicture.asset(
                  'assets/svgs/user.svg',
                  width: 4.w,
                  height: 4.h,
                ),
                onPress: () {
                  setState(() {
                    _nameHintText = '';
                  });
                },
                textEditingController: _nameCon,
                focusNode: _nameFocusNode,
                textInputType: TextInputType.text,
                acceptedColor: ThemesApp.kGreenColor,
              ),
              SizedBox(
                height: 2.h,
              ),

              GenderRadioButton(genderHasError: _genderHasError, onSelect: (String val) {
                setState(() {
                  _gender = val;
                });
              }),

              SizedBox(
                height: 1.h,
              ),
              CustomTextField(
                hasError: _workHasError,
                validator: (val) {
                  if (_workCon.text == '') {
                    setState(() {
                      _workHasError = true;
                    });
                    return 'Working field required'.tr;
                  }
                  setState(() {
                    _workHasError = false;
                  });
                  return null;
                },
                hintText: _workHintText,
                labelText: 'Working Field'.tr,
                suffixIcon: SvgPicture.asset(
                  'assets/svgs/user.svg',
                  width: 4.w,
                  height: 4.h,
                ),
                onPress: () {
                  setState(() {
                    _workHintText = '';
                  });
                },
                textEditingController: _workCon,
                focusNode: _workFocusNode,
                textInputType: TextInputType.text, acceptedColor: ThemesApp.kGreenColor,
              ),
              SizedBox(
                height: 3.h,
              ),
              CustomReadOnlyTextField(
                hintText: '04/05/2000',
                labelText: 'Birthday'.tr,
                suffixIcon: 'calendar.svg',
                onPress: () {
                  setState(() {
                    _birthdayHintText = '';
                  });
                },
                focusNode: _birthdayFocusNode,
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.w),
                child: Hero(
                  tag: 'next',
                  child: CustomGradientButton(
                    title: 'NEXT'.tr,
                    icon: 'assets/svgs/arrow-right-white.svg',
                    onPress: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      final isValid = _formKey.currentState!.validate();
                      if (_gender.isEmpty){
                        setState(() {
                          _genderHasError = true;
                        });
                      }
                      else{
                        _genderHasError = false;
                      }
                      if (isValid && !_genderHasError) {
                        Get.toNamed(
                          AppRouter.kSignUpSecondScreen,
                            arguments: SignUpArgs(fullName: _nameCon.text, gender: _gender, work: _workCon.text, birthday: '2000-02-02')
                        );
                      }
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  UnderlinePressedText(
                    title: 'Already have an account?',
                    onPress: () {
                      Get.toNamed(
                        AppRouter.kSignInScreen,
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}


