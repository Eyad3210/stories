import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/core/params/send_code_params.dart';
import 'package:stories/src/Features/Auth/core/params/verify_account_params.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../core/args/otp_args.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/otp_widget.dart';
import '../widgets/sign_back_button.dart';

class OtpScreen extends StatefulWidget {
  final OtpArgs otpArgs;
  const OtpScreen({Key? key, required this.otpArgs}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  Future<void> sendCode() async {
    await BlocProvider.of<AuthCubit>(context).sendCode(
        params: SendCodeParams(
      email: widget.otpArgs.email,
      phone: widget.otpArgs.phone,
    ));
  }

  @override
  void initState() {
    super.initState();
    sendCode();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) async{
          if (state is SendCodeLoading) {
            showCircularProgressIndicatorInsideAlertView(context);
          }
          if (state is SendCodeError) {
            print('error');
          }
          if (state is SendCodeDone) {
            Navigator.pop(context);
            print('done');
          }
          if (state is VerifyAccountLoading) {
            showCircularProgressIndicatorInsideAlertView(context);
          }
          if (state is VerifyAccountError) {
            Navigator.pop(context);
            print('error');
          }
          if (state is VerifyAccountDone) {
            Navigator.pop(context);
            print('done');
            box.write(HAS_TOKEN, '1');
            await storage.write(key: TOKEN, value: widget.otpArgs.token);
            Get.offAllNamed(AppRouter.kSectionScreen);
          }
        },
        child: Scaffold(
          backgroundColor: ThemesApp.kWhiteColor,
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SignBackButton(),
                Padding(
                  padding: EdgeInsets.only(top: 8.h),
                  child: Center(
                      child: Hero(
                    tag: 'stories',
                    child: Image(
                      image: const AssetImage('assets/images/stories.png'),
                      width: 70.w,
                      height: 20.h,
                    ),
                  )),
                ),
                SizedBox(
                  height: 8.h,
                ),
                FormContainer(
                  email: widget.otpArgs.email,
                  phone: widget.otpArgs.phone,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class FormContainer extends StatefulWidget {
  final String email;
  final String phone;
  const FormContainer({
    super.key,
    required this.email,
    required this.phone,
  });

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  String _code = '';

  Future<void> sendCode() async {
    await BlocProvider.of<AuthCubit>(context).sendCode(
        params: SendCodeParams(
      email: widget.email,
      phone: widget.phone,
    ));
  }

  Future<void> verifyAccount() async {
    await BlocProvider.of<AuthCubit>(context).verifyAccount(
        params: VerifyAccountParams(
            code: _code, email: widget.email, phone: widget.phone));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: ThemesApp.kDarkGrayColor,
            blurRadius: 10.0,
            spreadRadius: -7,
            offset: Offset(0, -10),
          ),
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.h), topRight: Radius.circular(5.h)),
        color: Colors.white,
      ),
      child: Padding(
        padding: EdgeInsets.only(
            top: 5.h,
            right: 5.w,
            left: 5.w,
            bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Verification Code'.tr,
              style: kBoldTextStyle.copyWith(fontSize: 25.sp),
            ),
            Text(
              'Enter the 5-digit verification code sent to your email'.tr,
              style: kRegularTextStyle.copyWith(fontSize: 15.sp),
            ),
            Text(
              widget.email,
              style: kBoldTextStyle.copyWith(fontSize: 15.sp),
            ),
            SizedBox(
              height: 3.h,
            ),
            OtpCode(
              onComplete: (String? value) {
                setState(() {
                  _code = value!;
                });
              },
              onChanged: (String? value) {},
            ),
            CountdownTimer(
              seconds: 30,
              onPress: sendCode,
            ),
            SizedBox(
              height: 4.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w),
              child: Hero(
                tag: 'next',
                child: CustomGradientButton(
                  title: 'CONFIRM'.tr,
                  onPress: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                    if (_code.length == 5) {
                      _code = '';
                      print(_code);
                      verifyAccount();
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CountdownTimer extends StatefulWidget {
  final int seconds;
  final VoidCallback onPress;
  const CountdownTimer(
      {super.key, required this.seconds, required this.onPress});

  @override
  _CountdownTimerState createState() => _CountdownTimerState();
}

class _CountdownTimerState extends State<CountdownTimer> {
  late Timer _timer;
  int _timeRemaining = 0;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  void startTimer() {
    _timeRemaining = widget.seconds;
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_timeRemaining < 1) {
            timer.cancel();
          } else {
            _timeRemaining = _timeRemaining - 1;
          }
        },
      ),
    );
  }

  void resetTimer() {
    setState(() {
      _timeRemaining = 30;
      _timer.cancel();
    });
  }

  String formatTime(int seconds) {
    int minutes = (seconds / 60).floor();
    int remainingSeconds = seconds - (minutes * 60);
    String minutesStr = minutes < 10 ? '0$minutes' : '$minutes';
    String secondsStr =
        remainingSeconds < 10 ? '0$remainingSeconds' : '$remainingSeconds';
    return '$minutesStr:$secondsStr';
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Didn\'t receive the verification code?'.tr,
              style: kRegularTextStyle.copyWith(fontSize: 15.sp),
            ),
            SizedBox(
              width: 3.w,
            ),
            if (_timeRemaining != 0)
              Text(
                'Resend'.tr,
                style: kRegularTextStyle.copyWith(
                  fontWeight: FontWeight.bold,
                  color: ThemesApp.kLightGrayColor,
                  fontSize: 15.sp,
                  decoration: TextDecoration.underline,
                  decorationColor: ThemesApp.kLightGrayColor,
                  decorationThickness: 1.0,
                  decorationStyle: TextDecorationStyle.solid,
                ),
              )
            else
              InkWell(
                onTap: () {
                  widget.onPress();
                  resetTimer();
                  startTimer();
                },
                child: Text(
                  'Resend'.tr,
                  style: kRegularTextStyle.copyWith(
                    fontWeight: FontWeight.bold,
                    color: ThemesApp.kMintColor,
                    fontSize: 15.sp,
                    decoration: TextDecoration.underline,
                    decorationColor: ThemesApp.kMintColor,
                    decorationThickness: 1.0,
                    decorationStyle: TextDecorationStyle.solid,
                  ),
                ),
              ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.timer_outlined,
              color: ThemesApp.kMintColor,
              size: 6.w,
            ),
            SizedBox(
              width: 1.w,
            ),
            Text(
              formatTime(_timeRemaining),
              style: kRegularTextStyle.copyWith(
                  fontSize: 15.sp, color: ThemesApp.kMintColor),
            ),
          ],
        ),
      ],
    );
  }
}
