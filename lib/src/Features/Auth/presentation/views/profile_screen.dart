import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Shared/presentation/widgets/custom_gradient_button.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../widgets/profile_item.dart';
import '../widgets/user_image_widget.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _notificationToggle = false;
  String _notificationToggleTest = 'OFF'.tr;

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is LogOutLoading){
          showCircularProgressIndicatorInsideAlertView(context);
        }
        if (state is LogOutError){
          Navigator.pop(context);
          print('error');
        }
        if (state is LogOutDone){
          Navigator.pop(context);
          print(state.data.data);
          box.remove(HAS_TOKEN);
          storage.delete(key: TOKEN);
          Get.offAllNamed(AppRouter.kHomeScreen);
        }
      },
      child: Padding(
        padding: EdgeInsets.only(top: 3.w),
        child: SafeArea(
            child: Scaffold(
          backgroundColor: ThemesApp.kWhiteColor,
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(right: 7.w, left: 7.w,),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const ProfileSection(),
                  SizedBox(
                    height: 3.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Settings'.tr,
                        style: kBoldTextStyle.copyWith(fontSize: 18.sp),
                      ),
                      ProfileItem(
                        title: 'Language'.tr,
                        icon: 'assets/svgs/language.svg',
                        onPress: () {},
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: ProfileItem(
                              title: 'Notifications'.tr,
                              icon: 'assets/svgs/notification.svg',
                              onPress: () {},
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              AnimatedSwitcher(
                                duration: const Duration(milliseconds: 500),
                                child: Text(
                                  key: ValueKey(_notificationToggleTest),
                                  _notificationToggleTest,
                                  style: kRegularTextStyle.copyWith(
                                      fontSize: 18.sp, color: ThemesApp.kBlackColor),
                                ),
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              FlutterSwitch(
                                  borderRadius: 4.h,
                                  padding: 1.w,
                                  toggleSize: 5.w,
                                  width: 13.w,
                                  height: 4.h,
                                  activeColor: ThemesApp.kLightYellowColor,
                                  inactiveColor: ThemesApp.kLightGrayColor,
                                  value: _notificationToggle,
                                  onToggle: (value) {
                                    setState(() {
                                      _notificationToggle = value;
                                      _notificationToggleTest =
                                          _notificationToggle
                                              ? 'ON'.tr
                                              : 'OFF'.tr;
                                    });
                                  })
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Help & Support'.tr,
                        style: kBoldTextStyle.copyWith(fontSize: 18.sp),
                      ),
                      ProfileItem(
                        title: 'Have a complaint?'.tr,
                        icon: 'assets/svgs/complaint.svg',
                        onPress: () {},
                      ),
                      ProfileItem(
                        title: 'Contact us'.tr,
                        icon: 'assets/svgs/contact-us.svg',
                        onPress: () {},
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )),
      ),
    );
  }
}

class ProfileSection extends StatefulWidget {
  const ProfileSection({Key? key}) : super(key: key);

  @override
  State<ProfileSection> createState() => _ProfileSectionState();
}

class _ProfileSectionState extends State<ProfileSection> {
  Future<void> getProfile() async {
    if (hasToken()) {
      await BlocProvider.of<AuthCubit>(context).getProfile();
    }
  }

  Future<void> logOut() async {
    await BlocProvider.of<AuthCubit>(context).logOut();
  }

  @override
  void initState() {
    super.initState();
    getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return hasToken()
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    overlayColor:
                        MaterialStateProperty.all<Color>(Colors.transparent),
                    onTap: () {
                      logOut();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                              color: ThemesApp.kDarkGrayColor,
                              blurRadius: 1.0,
                              spreadRadius: .1,
                              offset: Offset(0, 0),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(2.h),
                              topLeft: Radius.circular(2.h),
                              bottomLeft: Radius.circular(2.h),
                              bottomRight: Radius.circular(2.h))),
                      child: Padding(
                        padding: EdgeInsets.all(1.w),
                        child: SvgPicture.asset(
                          'assets/svgs/logout.svg',
                          width: 5.w,
                          height: 4.h,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              BlocBuilder<AuthCubit, AuthState>(
                builder: (context, state) {
                  if (state is GetProfileLoading) {
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 2.h),
                      child: const LinearProgressIndicator(),
                    );
                  }
                  if (state is GetProfileError) {
                    return const Text('error');
                  }
                  if (state is GetProfileDone) {
                    return Row(
                      children: [
                        UserImageWidget(
                          width: 7.w,
                          height: 15.w,
                        ),
                        SizedBox(
                          width: 3.w,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                state.signModel.data.name,
                                style: kBoldTextStyle.copyWith(fontSize: 18.sp),
                              ),
                              Text(
                                state.signModel.data.email,
                                style: kRegularTextStyle.copyWith(
                                    fontSize: 18.sp, color: ThemesApp.kGrayColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }
                  return const SizedBox();
                },
              ),
              SizedBox(
                height: 3.h,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Profile'.tr,
                    style: kBoldTextStyle.copyWith(fontSize: 18.sp),
                  ),
                  ProfileItem(
                    title: 'Manage your account'.tr,
                    icon: 'assets/svgs/manage-account.svg',
                    onPress: () {
                      Get.toNamed(AppRouter.kManageAccountScreen)!
                          .whenComplete(() => getProfile());
                    },
                  ),
                  ProfileItem(
                    title: 'My hourly packages'.tr,
                    icon: 'assets/svgs/ticket-star.svg',
                    onPress: () {
                      Get.toNamed(AppRouter.kMyHourlyPackagesScreen);
                    },
                  ),
                  ProfileItem(
                    title: 'My coupons'.tr,
                    icon: 'assets/svgs/coupons.svg',
                    onPress: () {},
                  ),
                  ProfileItem(
                    title: 'My internet bundles'.tr,
                    icon: 'assets/svgs/wifi-package.svg',
                    onPress: () {},
                  ),
                  ProfileItem(
                    title: 'My favourites'.tr,
                    icon: 'assets/svgs/favourites.svg',
                    onPress: () {},
                  ),
                ],
              ),
            ],
          )
        : Padding(
            padding: EdgeInsets.symmetric(vertical: 6.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('You are not registered yet'.tr,
                    style: kMediumTextStyle.copyWith(fontSize: 18.sp)),
                SizedBox(
                  height: 2.h,
                ),
                Text('Join Stories World Now!'.tr,
                    style: kBoldTextStyle.copyWith(fontSize: 22.sp)),
                SizedBox(
                  height: 2.h,
                ),
                Text('Enjoy a rich experience by logging in now'.tr,
                    style: kMediumTextStyle.copyWith(fontSize: 18.sp)),
                SizedBox(
                  height: 3.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 6.w),
                  child: CustomGradientButton(
                      title: 'LOGIN / SIGN UP'.tr,
                      onPress: () {
                        Get.toNamed(AppRouter.kSignUpScreen);
                      },
                      icon: 'assets/svgs/login.svg',
                      isPrefix: true),
                )
              ],
            ),
          );
  }
}
