import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';

import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/sign_back_button.dart';

class EmailVerifyScreen extends StatefulWidget {
  const EmailVerifyScreen({Key? key}) : super(key: key);

  @override
  State<EmailVerifyScreen> createState() => _EmailVerifyScreenState();
}

class _EmailVerifyScreenState extends State<EmailVerifyScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SignBackButton(),
              Padding(
                padding: EdgeInsets.only(top: 12.h),
                child: Center(
                    child: Hero(
                      tag: 'stories',
                      child: Image(
                        image: const AssetImage('assets/images/stories.png'),
                        width: 70.w,
                        height: 20.h,
                      ),
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 12.h,
                ),
                child: Center(
                    child: Text(
                      'Verify Your Account'.tr,
                      style: kBoldTextStyle.copyWith(fontSize: 25.sp),
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 1.h,
                  bottom: 2.h,
                ),
                child: Center(
                    child: Text(
                      'Enter the required information below please'.tr,
                      style: kRegularTextStyle.copyWith(fontSize: 15.sp),
                    )),
              ),
              const FormContainer(),
            ],
          ),
        ),
      ),
    );
  }
}

class FormContainer extends StatefulWidget {
  const FormContainer({
    super.key,
  });

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  String _emailHintText = 'E-mail'.tr;

  final TextEditingController _emailCon = TextEditingController();

  final FocusNode _emailFocusNode = FocusNode();

  bool _emailHasError = false;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: ThemesApp.kDarkGrayColor,
            blurRadius: 10.0,
            spreadRadius: -7,
            offset: Offset(0, -10),
          ),
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.h), topRight: Radius.circular(5.h)),
        color: Colors.white,
      ),
      child: Padding(
        padding: EdgeInsets.only(
            top: 5.h,
            right: 5.w,
            left: 5.w,
            bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Text(
                'a verification code will be sent to the email you will enter'.tr,
                style: kRegularTextStyle.copyWith(fontSize: 15.sp),
              ),
              SizedBox(height: 3.h,),
              CustomTextField(
                validator: (val){
                  if (_emailCon.text == '') {
                    setState(() {
                      _emailHasError = true;
                    });
                    return 'E-mail required'.tr;
                  }
                  final regex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
                  if (!regex.hasMatch(val!)) {
                    setState(() {
                      _emailHasError = true;
                    });
                    return 'Oops! wrong email form'.tr;
                  }
                  setState(() {
                    _emailHasError = false;
                  });
                  return null;
                },
                hintText: _emailHintText,
                labelText: 'E-mail'.tr,
                suffixIcon: SvgPicture.asset(
                  'assets/svgs/email.svg',
                  width: 4.w,
                  height: 4.h,
                ),
                onPress: () {
                  setState(() {
                    _emailHintText = '';
                  });
                },
                textEditingController: _emailCon, focusNode: _emailFocusNode,
                textInputType: TextInputType.emailAddress, hasError: _emailHasError, acceptedColor: ThemesApp.kGreenColor,),

              SizedBox(height: 4.h,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.w),
                child: Hero(
                  tag: 'next',
                  child: CustomGradientButton(
                    title: 'SEND VERIFICATION CODE'.tr,
                    onPress: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      final isValid = _formKey.currentState!.validate();
                      if (isValid){
                        Get.toNamed(AppRouter.kOtpScreen, arguments: _emailCon.text);
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
