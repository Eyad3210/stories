import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/core/args/otp_args.dart';
import 'package:stories/src/Features/Auth/core/params/sign_in_params.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/sign_back_button.dart';
import '../widgets/underline_pressed_text.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SignBackButton(),
              Padding(
                padding: EdgeInsets.only(top: 12.h),
                child: Center(
                  child: Hero(
                    tag: 'stories',
                    child: Image(
                      image: const AssetImage('assets/images/stories.png'),
                      width: 70.w,
                      height: 20.h,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 4.h,
                ),
                child: Center(
                    child: Text(
                  'Welcome Back!'.tr,
                  style: kBoldTextStyle.copyWith(fontSize: 25.sp),
                )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 1.h,
                  bottom: 2.h,
                ),
                child: Center(
                    child: Text(
                  'Enter the required information below please'.tr,
                  style: kRegularTextStyle.copyWith(fontSize: 15.sp),
                )),
              ),
              const FormContainer(),
            ],
          ),
        ),
      ),
    );
  }
}

class FormContainer extends StatefulWidget {
  const FormContainer({
    super.key,
  });

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  String _phoneHintText = 'Mobile Number'.tr;
  String _passwordHintText = 'Password'.tr;

  final TextEditingController _phoneCon = TextEditingController();
  final TextEditingController _passwordCon = TextEditingController();

  final FocusNode _phoneFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  bool _phoneHasError = false;
  bool _passwordHasError = false;

  bool _hidePass = true;

  final _formKey = GlobalKey<FormState>();

  Future<void> signIn() async {
    await BlocProvider.of<AuthCubit>(context).signIn(
        params:
            SignInParams(phone: _phoneCon.text, password: _passwordCon.text));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) async {
        if (state is SignInLoading) {
          showCircularProgressIndicatorInsideAlertView(context);
        }
        if (state is SignInError) {
          Navigator.pop(context);
          print(state.error);
        }
        if (state is SignInDone) {
          Navigator.pop(context);
          print(state.signModel.data.token);
          if (state.signModel.data.user.emailVerifiedAt == null) {
            Get.toNamed(
              AppRouter.kOtpScreen,
              arguments: OtpArgs(
                  email: state.signModel.data.user.email,
                  phone: state.signModel.data.user.phone,
                  token: state.signModel.data.token),
            );
          } else {
            box.write(HAS_TOKEN, '1');
            await storage.write(key: TOKEN, value: state.signModel.data.token);
            Get.offAllNamed(
              AppRouter.kSectionScreen,
            );
          }
        }
      },
      child: Container(
        decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              color: ThemesApp.kDarkGrayColor,
              blurRadius: 10.0,
              spreadRadius: -7,
              offset: Offset(0, -10),
            ),
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5.h), topRight: Radius.circular(5.h)),
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.only(
              top: 5.h,
              right: 5.w,
              left: 5.w,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextField(
                  validator: (val) {
                    if (_phoneCon.text == '') {
                      setState(() {
                        _phoneHasError = true;
                      });
                      return 'Mobile number required'.tr;
                    }
                    if (_phoneCon.text.length != 9) {
                      setState(() {
                        _phoneHasError = true;
                      });
                      return 'The mobile number must contain 9 digits, please check it again...'
                          .tr;
                    }
                    setState(() {
                      _phoneHasError = false;
                    });
                    return null;
                  },
                  hintText: _phoneHintText,
                  labelText: 'Mobile Number'.tr,
                  suffixIcon: SvgPicture.asset(
                    'assets/svgs/mobile.svg',
                    width: 4.w,
                    height: 4.h,
                  ),
                  onPress: () {
                    setState(() {
                      _phoneHintText = '';
                    });
                  },
                  textEditingController: _phoneCon,
                  focusNode: _phoneFocusNode,
                  textInputType: TextInputType.number,
                  hasError: _phoneHasError,
                  isPhoneNumber: true,
                  acceptedColor: ThemesApp.kGreenColor,
                ),
                SizedBox(
                  height: 3.h,
                ),
                CustomTextField(
                  validator: (val) {
                    if (_passwordCon.text == '') {
                      setState(() {
                        _passwordHasError = true;
                      });
                      return 'Password required'.tr;
                    }
                    if (_passwordCon.text.length < 8) {
                      setState(() {
                        _passwordHasError = true;
                      });
                      return 'Sorry! password must contain at least 8 characters'
                          .tr;
                    }
                    setState(() {
                      _passwordHasError = false;
                    });
                    return null;
                  },
                  hintText: _passwordHintText,
                  labelText: 'Password'.tr,
                  obscure: _hidePass,
                  suffixIcon: _passwordFocusNode.hasFocus
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              _hidePass = !_hidePass;
                            });
                          },
                          child: SvgPicture.asset(
                            _hidePass
                                ? 'assets/svgs/eye-slash.svg'
                                : 'assets/svgs/eye.svg',
                            width: 4.w,
                            height: 4.h,
                          ),
                        )
                      : SvgPicture.asset(
                          'assets/svgs/lock.svg',
                          width: 4.w,
                          height: 4.h,
                        ),
                  onPress: () {
                    setState(() {
                      _passwordHintText = '';
                    });
                  },
                  textEditingController: _passwordCon,
                  focusNode: _passwordFocusNode,
                  textInputType: TextInputType.visiblePassword,
                  hasError: _passwordHasError,
                  acceptedColor: ThemesApp.kGreenColor,
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: ThemesApp.kGrayColor, width: 2),
                            borderRadius: BorderRadius.circular(20.h)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: .5.h, horizontal: 1.w),
                          child: Text(
                            'Forget password'.tr,
                            style: kMediumTextStyle.copyWith(
                                fontSize: 16.sp, color: ThemesApp.kGrayColor),
                          ),
                        )),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 6.w),
                  child: CustomGradientButton(
                    title: 'LOGIN'.tr,
                    onPress: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      final isValid = _formKey.currentState!.validate();
                      if (isValid) {
                        signIn();
                      }
                    },
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    UnderlinePressedText(
                      title: 'Don\'t have an account?'.tr,
                      onPress: () {
                        Get.toNamed(AppRouter.kSignInScreen);
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
