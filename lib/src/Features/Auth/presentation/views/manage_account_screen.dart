import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/core/params/edit_profile_params.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Features/Auth/presentation/widgets/custom_text_field.dart';
import 'package:stories/src/Shared/core/util/constants.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/presentation/widgets/custom_app_bar.dart';
import '../../../../Shared/presentation/widgets/custom_button.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/custom_read_only_text_field.dart';
import '../widgets/gender_radio_button.dart';
import '../widgets/user_image_widget.dart';

class ManageAccountScreen extends StatefulWidget {
  const ManageAccountScreen({Key? key}) : super(key: key);

  @override
  State<ManageAccountScreen> createState() => _ManageAccountScreenState();
}

class _ManageAccountScreenState extends State<ManageAccountScreen> {
  Future<void> getProfile() async {
    await BlocProvider.of<AuthCubit>(context).getProfile();
  }

  @override
  void initState() {
    super.initState();
    getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: BlocConsumer<AuthCubit, AuthState>(
          listener: (context, state) {
            if (state is EditProfileLoading){
              showCircularProgressIndicatorInsideAlertView(context);
            }
            if (state is EditProfileError){
              Navigator.pop(context);
              print('error');
            }
            if (state is EditProfileDone){
              Navigator.pop(context);
              getProfile();
            }
          },
          builder: (context, state) {
            if (state is GetProfileLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is GetProfileError) {
              print(state.error);
              return Container();
            }
            if (state is GetProfileDone) {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(
                    top: 2.h,
                    left: 4.w,
                    right: 4.w,
                    bottom: MediaQuery.of(context).viewInsets.bottom,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomAppBar(
                        title: 'Personal information'.tr,
                      ),
                      SizedBox(
                        height: 4.h,
                      ),
                      UserImageWidget(
                        width: 7.w,
                        height: 25.w,
                      ),
                      Text(
                        state.signModel.data.email,
                        style: kRegularTextStyle.copyWith(
                            fontSize: 18.sp, color: ThemesApp.kGrayColor),
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      EditForm(
                        name: state.signModel.data.name,
                        phone: state.signModel.data.phone.substring(3, 12),
                        work: state.signModel.data.work,
                        gender: state.signModel.data.gender == 'M' ? 0 : 1,
                      ),
                    ],
                  ),
                ),
              );
            }
            return const SizedBox();
          },
        ),
      ),
    );
  }
}

class EditForm extends StatefulWidget {
  final String name;
  final String phone;
  final String work;
  final int gender;

  const EditForm({
    Key? key,
    required this.name,
    required this.phone,
    required this.work,
    required this.gender,
  }) : super(key: key);

  @override
  State<EditForm> createState() => _EditFormState();
}

class _EditFormState extends State<EditForm> {
  final TextEditingController _nameCon = TextEditingController();
  final TextEditingController _workCon = TextEditingController();
  final TextEditingController _phoneCon = TextEditingController();

  String _nameHintText = 'Full Name'.tr;
  String _workHintText = 'Working Field'.tr;
  String _phoneHintText = 'Mobile Number'.tr;
  String _birthdayHintText = 'Birthday'.tr;
  String _gender = '';

  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _workFocusNode = FocusNode();
  final FocusNode _phoneFocusNode = FocusNode();
  final FocusNode _birthdayFocusNode = FocusNode();

  bool _nameHasError = false;
  bool _workHasError = false;
  bool _phoneHasError = false;
  bool _genderHasError = false;

  final _formKey = GlobalKey<FormState>();

  Future<void> editProfile() async {
    print('EDIT');
    print(_phoneCon.text);
    await BlocProvider.of<AuthCubit>(context).editProfile(
        params: EditProfileParams(
            name: widget.name == _nameCon.text ? null : _nameCon.text,
            gender: _gender == 'Male'.tr ? 'M' : 'F',
            work: widget.work == _workCon.text ? null :_workCon.text,
            birthday: "2000-02-02",
            phone: widget.phone == _phoneCon.text ? null : _phoneCon.text,
            email: null));
  }

  @override
  void initState() {
    super.initState();
    _nameCon.text = widget.name;
    _phoneCon.text = widget.phone;
    _workCon.text = widget.work;
    if (widget.gender == 0) {
      _gender = 'Male'.tr;
    } else {
      _gender = 'Female'.tr;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextField(
            hasError: _nameHasError,
            validator: (val) {
              if (_nameCon.text == '') {
                setState(() {
                  _nameHasError = true;
                });
                return 'Full name required'.tr;
              }
              setState(() {
                _nameHasError = false;
              });
              return null;
            },
            hintText: _nameHintText,
            labelText: 'Full Name'.tr,
            suffixIcon: SvgPicture.asset(
              'assets/svgs/user.svg',
              width: 4.w,
              height: 4.h,
            ),
            onPress: () {
              setState(() {
                _nameHintText = '';
              });
            },
            textEditingController: _nameCon,
            focusNode: _nameFocusNode,
            textInputType: TextInputType.text,
            acceptedColor: ThemesApp.kPurpleColor,
          ),
          SizedBox(
            height: 2.h,
          ),
          CustomTextField(
            validator: (val) {
              if (_phoneCon.text == '') {
                setState(() {
                  _phoneHasError = true;
                });
                return 'Mobile number required'.tr;
              }
              if (_phoneCon.text.length != 9) {
                setState(() {
                  _phoneHasError = true;
                });
                return 'The mobile number must contain 9 digits, please check it again...'
                    .tr;
              }
              setState(() {
                _phoneHasError = false;
              });
              return null;
            },
            hintText: _phoneHintText,
            labelText: 'Mobile Number'.tr,
            suffixIcon: SvgPicture.asset(
              'assets/svgs/mobile.svg',
              width: 4.w,
              height: 4.h,
            ),
            onPress: () {
              setState(() {
                _phoneHintText = '';
              });
            },
            textEditingController: _phoneCon,
            focusNode: _phoneFocusNode,
            textInputType: TextInputType.number,
            hasError: _phoneHasError,
            isPhoneNumber: true,
            acceptedColor: ThemesApp.kPurpleColor,
          ),
          SizedBox(
            height: 2.h,
          ),
          CustomReadOnlyTextField(
            hintText: _birthdayHintText,
            labelText: 'Birthday'.tr,
            suffixIcon: 'calendar.svg',
            onPress: () {
              setState(() {
                _birthdayHintText = '';
              });
            },
            focusNode: _birthdayFocusNode,
          ),
          SizedBox(
            height: 2.h,
          ),
          CustomTextField(
            hasError: _workHasError,
            validator: (val) {
              if (_workCon.text == '') {
                setState(() {
                  _workHasError = true;
                });
                return 'Working field required'.tr;
              }
              setState(() {
                _workHasError = false;
              });
              return null;
            },
            hintText: _workHintText,
            labelText: 'Working Field'.tr,
            suffixIcon: SvgPicture.asset(
              'assets/svgs/user.svg',
              width: 4.w,
              height: 4.h,
            ),
            onPress: () {
              setState(() {
                _workHintText = '';
              });
            },
            textEditingController: _workCon,
            focusNode: _workFocusNode,
            textInputType: TextInputType.text,
            acceptedColor: ThemesApp.kPurpleColor,
          ),
          SizedBox(
            height: 2.h,
          ),
          GenderRadioButton(
              initValue: widget.gender,
              genderHasError: _genderHasError,
              onSelect: (String val) {
                setState(() {
                  _gender = val;
                });
              }),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            child: CustomButton(
              borderColor: ThemesApp.kMintColor,
              textColor: ThemesApp.kBlackColor,
              iconColor: ThemesApp.kMintColor,
              icon: 'assets/svgs/lock.svg',
              title: 'CHANGE YOUR PASSWORD'.tr,
              onPress: () {
                FocusManager.instance.primaryFocus?.unfocus();
                final isValid = _formKey.currentState!.validate();
                if (_gender.isEmpty) {
                  setState(() {
                    _genderHasError = true;
                  });
                } else {
                  _genderHasError = false;
                }
                if (isValid && !_genderHasError) {
                  print('done');
                }
              }, backgroundColor: ThemesApp.kWhiteColor,
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            child: CustomGradientButton(
              title: 'SAVE CHANGES'.tr,
              onPress: () {
                FocusManager.instance.primaryFocus?.unfocus();
                final isValid = _formKey.currentState!.validate();
                if (_gender.isEmpty) {
                  setState(() {
                    _genderHasError = true;
                  });
                } else {
                  _genderHasError = false;
                }
                if (isValid && !_genderHasError) {
                  editProfile();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
