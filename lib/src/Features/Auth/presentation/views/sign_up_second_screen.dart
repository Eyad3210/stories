import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/core/args/otp_args.dart';
import 'package:stories/src/Features/Auth/core/args/sign_up_args.dart';
import 'package:stories/src/Features/Auth/core/params/sign_up_params.dart';
import 'package:stories/src/Features/Auth/presentation/blocs/auth_cubit.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';
import '../../../../Shared/presentation/widgets/custom_gradient_button.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/sign_back_button.dart';

class SignUpSecondScreen extends StatefulWidget {
  const SignUpSecondScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<SignUpSecondScreen> createState() => _SignUpSecondScreenState();
}

class _SignUpSecondScreenState extends State<SignUpSecondScreen> {
  final SignUpArgs signUpArgs = Get.arguments as SignUpArgs;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemesApp.kWhiteColor,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SignBackButton(),
              Padding(
                padding: EdgeInsets.only(top: 10.h),
                child: Center(
                  child: Hero(
                    tag: 'stories',
                    child: Image(
                      image: const AssetImage('assets/images/stories.png'),
                      width: 70.w,
                      height: 20.h,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 5.h,
                ),
                child: Center(
                    child: Text(
                  'Create Your Account!'.tr,
                  style: kBoldTextStyle.copyWith(fontSize: 25.sp),
                )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 1.h,
                  bottom: 2.h,
                ),
                child: Center(
                    child: Text(
                  'Enter the required information below please'.tr,
                  style: kRegularTextStyle.copyWith(fontSize: 15.sp),
                )),
              ),
              FormContainer(
                signUpArgs: signUpArgs,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FormContainer extends StatefulWidget {
  final SignUpArgs signUpArgs;
  const FormContainer({
    super.key,
    required this.signUpArgs,
  });

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  String _emailHintText = 'E-mail'.tr;
  String _phoneHintText = 'Mobile Number'.tr;
  String _passwordHintText = 'Password'.tr;

  final TextEditingController _emailCon = TextEditingController();
  final TextEditingController _phoneCon = TextEditingController();
  final TextEditingController _passwordCon = TextEditingController();

  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _phoneFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  bool _emailHasError = false;
  bool _phoneHasError = false;
  bool _passwordHasError = false;

  bool _hidePass = true;

  final _formKey = GlobalKey<FormState>();

  Future<void> signUp() async {
    await BlocProvider.of<AuthCubit>(context).signUp(
        params: SignUpParams(
            name: widget.signUpArgs.fullName,
            email: _emailCon.text,
            phone: _phoneCon.text,
            password: _passwordCon.text,
            gender: widget.signUpArgs.gender == 'Male'.tr ? 'M' : 'F',
            work: widget.signUpArgs.work,
            birthday: widget.signUpArgs.birthday,
            deviceToken: 'token'));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is SignUpLoading) {
          showCircularProgressIndicatorInsideAlertView(context);
        }
        if (state is SignUpError) {
          Navigator.pop(context);
          print(state.error);
        }
        if (state is SignUpDone) {
          Navigator.pop(context);
          print(state.signModel.data.token);
          Get.toNamed(
            AppRouter.kOtpScreen,
            arguments: OtpArgs(
              email: _emailCon.text,
              phone: _phoneCon.text,
              token: state.signModel.data.token,
            ),
          );
        }
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: const [
              BoxShadow(
                color: ThemesApp.kDarkGrayColor,
                blurRadius: 10.0,
                spreadRadius: -7,
                offset: Offset(0, -10),
              ),
            ],
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(5.h), topLeft: Radius.circular(5.h))),
        child: Padding(
          padding: EdgeInsets.only(
              top: 5.h,
              right: 5.w,
              left: 5.w,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextField(
                  validator: (val) {
                    if (_emailCon.text == '') {
                      setState(() {
                        _emailHasError = true;
                      });
                      return 'E-mail required'.tr;
                    }
                    final regex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
                    if (!regex.hasMatch(val!)) {
                      setState(() {
                        _emailHasError = true;
                      });
                      return 'Oops! wrong email form'.tr;
                    }
                    setState(() {
                      _emailHasError = false;
                    });
                    return null;
                  },
                  hintText: _emailHintText,
                  labelText: 'E-mail'.tr,
                  suffixIcon: SvgPicture.asset(
                    'assets/svgs/email.svg',
                    width: 4.w,
                    height: 4.h,
                  ),
                  onPress: () {
                    setState(() {
                      _emailHintText = '';
                    });
                  },
                  textEditingController: _emailCon,
                  focusNode: _emailFocusNode,
                  textInputType: TextInputType.emailAddress,
                  hasError: _emailHasError,
                  acceptedColor: ThemesApp.kGreenColor,
                ),
                SizedBox(
                  height: 3.h,
                ),
                CustomTextField(
                  validator: (val) {
                    if (_phoneCon.text == '') {
                      setState(() {
                        _phoneHasError = true;
                      });
                      return 'Mobile number required'.tr;
                    }
                    if (_phoneCon.text.length != 9) {
                      setState(() {
                        _phoneHasError = true;
                      });
                      return 'The mobile number must contain 9 digits, please check it again...'
                          .tr;
                    }
                    setState(() {
                      _phoneHasError = false;
                    });
                    return null;
                  },
                  hintText: _phoneHintText,
                  labelText: 'Mobile Number'.tr,
                  suffixIcon: SvgPicture.asset(
                    'assets/svgs/mobile.svg',
                    width: 4.w,
                    height: 4.h,
                  ),
                  onPress: () {
                    setState(() {
                      _phoneHintText = '';
                    });
                  },
                  textEditingController: _phoneCon,
                  focusNode: _phoneFocusNode,
                  textInputType: TextInputType.number,
                  hasError: _phoneHasError,
                  acceptedColor: ThemesApp.kGreenColor,
                  isPhoneNumber: true,
                ),
                SizedBox(
                  height: 3.h,
                ),
                CustomTextField(
                  validator: (val) {
                    if (_passwordCon.text == '') {
                      setState(() {
                        _passwordHasError = true;
                      });
                      return 'Password required'.tr;
                    }
                    if (_passwordCon.text.length < 8) {
                      setState(() {
                        _passwordHasError = true;
                      });
                      return 'Sorry! password must contain at least 8 characters'
                          .tr;
                    }
                    setState(() {
                      _passwordHasError = false;
                    });
                    return null;
                  },
                  hintText: _passwordHintText,
                  labelText: 'Password'.tr,
                  obscure: _hidePass,
                  suffixIcon: _passwordFocusNode.hasFocus
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              _hidePass = !_hidePass;
                            });
                          },
                          child: SvgPicture.asset(
                            _hidePass
                                ? 'assets/svgs/eye-slash.svg'
                                : 'assets/svgs/eye.svg',
                            width: 4.w,
                            height: 4.h,
                          ),
                        )
                      : SvgPicture.asset(
                          'assets/svgs/lock.svg',
                          width: 4.w,
                          height: 4.h,
                        ),
                  onPress: () {
                    setState(() {
                      _passwordHintText = '';
                    });
                  },
                  textEditingController: _passwordCon,
                  focusNode: _passwordFocusNode,
                  textInputType: TextInputType.visiblePassword,
                  hasError: _passwordHasError,
                  acceptedColor: ThemesApp.kGreenColor,
                ),
                SizedBox(
                  height: 2.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 6.w),
                  child: Hero(
                    tag: 'next',
                    child: CustomGradientButton(
                      icon: 'assets/svgs/arrow-right-white.svg',
                      title: 'NEXT'.tr,
                      onPress: () {
                        FocusManager.instance.primaryFocus?.unfocus();
                        final isValid = _formKey.currentState!.validate();
                        if (isValid) {
                          signUp();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
