import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class ProfileItem extends StatelessWidget {
  final String title;
  final String icon;
  final VoidCallback onPress;

  const ProfileItem({
    super.key,
    required this.title,
    required this.icon,
    required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 1.w),
      child: InkWell(
        overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
        onTap: onPress,
        child: Row(
          children: [
            SvgPicture.asset(
              icon,
              width: 5.w,
              height: 4.h,
            ),
            SizedBox(
              width: 3.w,
            ),
            Expanded(
              child: Text(
                title,
                style: kRegularTextStyle.copyWith(
                    fontSize: 18.sp, color: ThemesApp.kBlackColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
