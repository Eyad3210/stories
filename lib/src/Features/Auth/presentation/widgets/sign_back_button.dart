import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class SignBackButton extends StatelessWidget {
  const SignBackButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
      onTap: (){
        Get.back();
      },
      child: Padding(
        padding: EdgeInsets.only(top: 2.h, left: 4.w),
        child: Hero(
          tag: 'arrow-left',
          child: SvgPicture.asset(
            'assets/svgs/arrow-left.svg',
            width: 2.w,
            height: 3.h,
          ),
        ),
      ),
    );
  }
}
