import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/presentation/widgets/radio_button.dart';

import '../../../../Shared/core/util/constants.dart';

class CustomRadio extends StatefulWidget {
  final List<String> options;
  final List<Color> optionsColors;
  final List<Widget> optionsIcons;
  final Function(String) onSelected;
  final int? initValue;

  const CustomRadio(
      {Key? key,
        required this.options,
        required this.onSelected,
        required this.optionsColors,
        required this.optionsIcons,
        this.initValue})
      : super(key: key);

  @override
  State<CustomRadio> createState() => _CustomRadioState();
}

class _CustomRadioState extends State<CustomRadio> {
  final List<int> _selectedOption = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.options.length; i++) {
      _selectedOption.add(-1);
    }
    if (widget.initValue != null){
      _selectedOption[widget.initValue!] = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(right: 8.w),
          child: InkWell(
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
            onTap: () {
              setState(() {
                for (int i = 0; i < _selectedOption.length; i++) {
                  if (i != index) {
                    _selectedOption[i] = -1;
                  } else {
                    _selectedOption[i] = 1;
                    widget.onSelected(widget.options.elementAt(i));
                  }
                }
              });
            },
            child: Row(
              children: [
                RadioButton(
                  isSelected: _selectedOption.elementAt(index),
                  index: index,
                  color: widget.optionsColors,
                ),
                SizedBox(width: 2.w,),
                Text(widget.options.elementAt(index), style: kRegularTextStyle.copyWith(fontSize: 16.sp),),
                SizedBox(width: 1.w,),
                widget.optionsIcons.elementAt(index),
              ],
            ),
          ),
        );
      },
      itemCount: widget.options.length,
    );
  }
}