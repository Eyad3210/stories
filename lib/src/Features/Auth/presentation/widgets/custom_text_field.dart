import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.hintText,
    required this.labelText,
    required this.suffixIcon,
    required this.textInputType,
    required this.textEditingController,
    this.obscure,
    required this.onPress,
    required this.focusNode,
    required this.validator,
    required this.hasError,
    this.isPhoneNumber,
    required this.acceptedColor,
  });

  final String hintText;
  final String labelText;
  final Widget suffixIcon;
  final TextEditingController textEditingController;
  final bool? obscure;
  final TextInputType textInputType;
  final VoidCallback onPress;
  final FocusNode focusNode;
  final String? Function(String?)? validator;
  final bool hasError;
  final bool? isPhoneNumber;
  final Color acceptedColor;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validator,
      keyboardType: textInputType,
      obscureText: obscure == null ? false : obscure!,
      focusNode: focusNode,
      controller: textEditingController,
      style: kMediumTextStyle.copyWith(
        fontSize: 18.sp,
        color: ThemesApp.kBlackColor
      ),
      onTap: onPress,
      decoration: InputDecoration(
        prefix: isPhoneNumber != null ? Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Text('963', style: kRegularTextStyle.copyWith(fontSize: 18.sp, color: ThemesApp.kBlackColor),),
        ) : null,
        suffixIcon: Padding(
          padding: EdgeInsets.only(right: 2.w),
          child: suffixIcon,
        ),
        suffixIconConstraints: BoxConstraints(maxHeight: 8.h, maxWidth: 8.w),
        labelText: labelText,
        labelStyle: kMediumTextStyle.copyWith(fontSize: 18.sp, color: hasError ? ThemesApp.kRedColor :  focusNode.hasFocus ? ThemesApp.kMintColor : textEditingController.text != '' ? acceptedColor : ThemesApp.kGrayColor),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(1.h),
          borderSide: BorderSide(
            color: textEditingController.text != '' ? acceptedColor : ThemesApp.kGrayColor,
            width: .25.w,
          ),
        ),
        errorStyle: kMediumTextStyle.copyWith(fontSize: 12.sp, color: ThemesApp.kRedColor),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(1.h),
          borderSide: BorderSide(
            color: ThemesApp.kRedColor,
            width: .5.w,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(1.h),
          borderSide: BorderSide(
            color: ThemesApp.kRedColor,
            width: .5.w,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(1.h),
          borderSide: BorderSide(
            color: ThemesApp.kMintColor,
            width: .5.w,
          ),
        ),
        hintText: hintText,
        hintStyle: kMediumTextStyle.copyWith(fontSize: 18.sp, color: ThemesApp.kDarkGrayColor),
        contentPadding: EdgeInsets.symmetric(
            vertical: 1.h,
            horizontal: 5.w), // adjust the vertical padding to lower the height
      ),
    );
  }
}
