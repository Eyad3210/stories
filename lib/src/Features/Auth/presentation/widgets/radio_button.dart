import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class RadioButton extends StatefulWidget {
  final int isSelected;
  final int index;
  final List<Color> color;
  const RadioButton({
    Key? key,
    required this.isSelected,
    required this.index,
    required this.color,
  }) : super(key: key);

  @override
  State<RadioButton> createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          child: widget.isSelected == -1
              ? Container(
            key: const ValueKey(1),
            child: Icon(
              Icons.radio_button_off,
              color: Colors.grey,
              size: 5.w,
            ),
          )
              : Container(
            key: const ValueKey(2),
            child: Icon(
              Icons.radio_button_checked,
              color: widget.color.elementAt(widget.index),
              size: 5.w,
            ),
          ),
          transitionBuilder: (child, animation) => FadeTransition(
            opacity: animation,
            child: child,
          ),
        ),
      ],
    );
  }
}