import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/core/util/constants.dart';

class CustomReadOnlyTextField extends StatelessWidget {
  const CustomReadOnlyTextField({
    super.key,
    required this.hintText,
    required this.labelText,
    required this.suffixIcon,
    required this.onPress,
    required this.focusNode,
  });

  final String hintText;
  final String labelText;
  final String suffixIcon;
  final VoidCallback onPress;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: TextField(
        readOnly: true,
        style: kMediumTextStyle.copyWith(
          fontSize: 18.sp,
        ),
        onTap: onPress,
        focusNode: focusNode,
        decoration: InputDecoration(
          suffixIcon: Padding(
            padding: EdgeInsets.only(right: 2.w),
            child: SvgPicture.asset(
              'assets/svgs/$suffixIcon',
              width: 4.w,
              height: 4.h,
            ),
          ),
          suffixIconConstraints: BoxConstraints(maxHeight: 8.h, maxWidth: 8.w),
          labelText: labelText,
          labelStyle: kMediumTextStyle.copyWith(fontSize: 18.sp, color: focusNode.hasFocus ? Colors.blue : Colors.grey),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(1.h),
            borderSide: BorderSide(
              color: Colors.grey,
              width: .25.w,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(1.h),
            borderSide: BorderSide(
              color: Colors.blue,
              width: .5.w,
            ),
          ),
          hintText: hintText,
          hintStyle: kMediumTextStyle.copyWith(fontSize: 18.sp),
          contentPadding: EdgeInsets.symmetric(
              vertical: 1.h,
              horizontal: 5.w), // adjust the vertical padding to lower the height
        ),
      ),
    );
  }
}
