import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Features/Auth/presentation/widgets/custom_radio.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class GenderRadioButton extends StatelessWidget {
  final bool genderHasError;
  final dynamic Function(String) onSelect;
  final int? initValue;
  const GenderRadioButton({Key? key, required this.genderHasError,required this.onSelect, this.initValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Gender'.tr,
          style: kMediumTextStyle.copyWith(fontSize: 18.sp),
        ),
        SizedBox(
          height: 6.h,
          child: CustomRadio(
            options: ['Male'.tr, 'Female'.tr],
            onSelected: onSelect,
            optionsColors: const [ThemesApp.kMintColor, ThemesApp.kPinkColor],
            optionsIcons: [
              SvgPicture.asset(
                'assets/svgs/male.svg',
                width: 5.w,
                height: 3.h,
              ),
              SvgPicture.asset(
                'assets/svgs/female.svg',
                width: 5.w,
                height: 3.h,
              )
            ], initValue: initValue,
          ),
        ),
        genderHasError == true ? Padding(
          padding: EdgeInsets.only(left: 4.w, bottom: 1.h),
          child: Text('Gender required'.tr, style: kMediumTextStyle.copyWith(fontSize: 12.sp, color: ThemesApp.kRedColor),),
        ) : const SizedBox(),
      ],
    );
  }
}
