import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';
import '../../../../Shared/core/util/constants.dart';

class UnderlinePressedText extends StatelessWidget {
  final String title;
  final VoidCallback onPress;

  const UnderlinePressedText({
    super.key, required this.title, required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Text(
        title,
        style: kMediumTextStyle.copyWith(
          fontSize: 18.sp,
          color: ThemesApp.kMintColor,
        ),
      ),);
  }
}
