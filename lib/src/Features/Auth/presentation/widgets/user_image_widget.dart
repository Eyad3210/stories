import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';

class UserImageWidget extends StatelessWidget {
  final double width;
  final double height;

  const UserImageWidget({
    super.key, required this.width, required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: ThemesApp.kPurpleColor,
          width: .8.w,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(1.w),
        child: SvgPicture.asset(
          'assets/svgs/purpelperson.svg',
          width: width,
          height: height,
        ),
      ),
    );
  }
}
