import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:sizer/sizer.dart';

import '../../../../Shared/config/theme/themes_app.dart';

class OtpCode extends StatelessWidget {
  final Function(String?) onComplete;
  final Function(String?) onChanged;

  const OtpCode({
    super.key,
    required this.onComplete,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      keyboardType: TextInputType.number,
      length: 5,
      obscureText: false,
      textStyle: TextStyle(
        fontSize: 20.sp,
        color: ThemesApp.kBlackColor,

      ),
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        activeColor: ThemesApp.kGrayColor,
        inactiveColor: ThemesApp.kGrayColor.withOpacity(.5),
        selectedFillColor: Colors.white,
        selectedColor: ThemesApp.kGrayColor,
        inactiveFillColor: Colors.white,
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(2.h),
        fieldHeight: 12.h,
        fieldWidth: 17.w,
        activeFillColor: Colors.white,
      ),
      animationDuration: const Duration(milliseconds: 300),
      enableActiveFill: true,
      onCompleted: onComplete,
      onChanged: onChanged,
      appContext: context,
    );
  }
}
