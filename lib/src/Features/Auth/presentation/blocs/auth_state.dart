part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthEmpty extends AuthState {
  const AuthEmpty();
}

// Sign up
class SignUpLoading extends AuthState {
  const SignUpLoading();
}

class SignUpDone extends AuthState {
  final GetSignModel signModel;

  const SignUpDone(this.signModel);

  @override
  List<Object> get props => [signModel];
}

class SignUpError extends AuthState {
  final DioError error;

  const SignUpError(this.error);

  @override
  List<Object> get props => [error];
}

// Sign in
class SignInLoading extends AuthState {
  const SignInLoading();
}

class SignInDone extends AuthState {
  final GetSignModel signModel;

  const SignInDone(this.signModel);

  @override
  List<Object> get props => [signModel];
}

class SignInError extends AuthState {
  final DioError error;

  const SignInError(this.error);

  @override
  List<Object> get props => [error];
}

//Log out
class LogOutLoading extends AuthState {
  const LogOutLoading();
}

class LogOutDone extends AuthState {
  final GetStringDataModel data;

  const LogOutDone(this.data);

  @override
  List<Object> get props => [data];
}

class LogOutError extends AuthState {
  final DioError error;

  const LogOutError(this.error);

  @override
  List<Object> get props => [error];
}

//Get profile
class GetProfileLoading extends AuthState {
  const GetProfileLoading();
}

class GetProfileDone extends AuthState {
  final GetUserModel signModel;

  const GetProfileDone(this.signModel);

  @override
  List<Object> get props => [signModel];
}

class GetProfileError extends AuthState {
  final DioError error;

  const GetProfileError(this.error);

  @override
  List<Object> get props => [error];
}

//Edit profile
class EditProfileLoading extends AuthState {
  const EditProfileLoading();
}

class EditProfileDone extends AuthState {
  final GetUserModel signModel;

  const EditProfileDone(this.signModel);

  @override
  List<Object> get props => [signModel];
}

class EditProfileError extends AuthState {
  final DioError error;

  const EditProfileError(this.error);

  @override
  List<Object> get props => [error];
}

//Send code
class SendCodeLoading extends AuthState {
  const SendCodeLoading();
}

class SendCodeDone extends AuthState {
  const SendCodeDone();
}

class SendCodeError extends AuthState {
  final DioError error;

  const SendCodeError(this.error);

  @override
  List<Object> get props => [error];
}

//Verify account
class VerifyAccountLoading extends AuthState {
  const VerifyAccountLoading();
}

class VerifyAccountDone extends AuthState {
  final GetUserModel signModel;

  const VerifyAccountDone(this.signModel);
}

class VerifyAccountError extends AuthState {
  final DioError error;

  const VerifyAccountError(this.error);

  @override
  List<Object> get props => [error];
}



