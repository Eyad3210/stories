
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:stories/src/Features/Auth/core/params/edit_profile_params.dart';
import 'package:stories/src/Features/Auth/core/params/sign_in_params.dart';
import 'package:stories/src/Features/Auth/core/params/sign_up_params.dart';
import 'package:stories/src/Features/Auth/core/params/verify_account_params.dart';
import 'package:stories/src/Features/Auth/data/models/sign_model.dart';
import 'package:stories/src/Features/Auth/data/repo/auth_repo.dart';
import 'package:stories/src/Shared/core/resources/data_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stories/src/Shared/data/models/string_data_model.dart';

import '../../core/params/send_code_params.dart';
import '../../data/models/user_model.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepo _authRepo;

  AuthCubit(this._authRepo) : super(const AuthEmpty());

  Future<void> signUp({required SignUpParams params}) async{
    emit(const SignUpLoading());
    final dataState = await _authRepo.signUp(params: params);
    if (dataState is DataSuccess){
      emit(SignUpDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(SignUpError(dataState.error!));
    }
  }

  Future<void> signIn({required SignInParams params}) async{
    emit(const SignInLoading());
    final dataState = await _authRepo.signIn(params: params);
    if (dataState is DataSuccess){
      emit(SignInDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(SignInError(dataState.error!));
    }
  }

  Future<void> logOut() async{
    emit(const LogOutLoading());
    final dataState = await _authRepo.logOut();
    if (dataState is DataSuccess){
      emit(LogOutDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(LogOutError(dataState.error!));
    }
  }

  Future<void> getProfile() async{
    emit(const GetProfileLoading());
    final dataState = await _authRepo.getProfile();
    if (dataState is DataSuccess){
      emit(GetProfileDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(GetProfileError(dataState.error!));
    }
  }

  Future<void> editProfile({required EditProfileParams params}) async{
    emit(const EditProfileLoading());
    final dataState = await _authRepo.editProfile(params: params);
    if (dataState is DataSuccess){
      emit(EditProfileDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(EditProfileError(dataState.error!));
    }
  }

  Future<void> sendCode({required SendCodeParams params}) async{
    emit(const SendCodeLoading());
    final dataState = await _authRepo.sendCode(params: params);
    if (dataState is DataSuccess){
      emit(const SendCodeDone());
    }
    if (dataState is DataFailed){
      emit(SendCodeError(dataState.error!));
    }
  }

  Future<void> verifyAccount({required VerifyAccountParams params}) async{
    emit(const VerifyAccountLoading());
    final dataState = await _authRepo.verifyAccount(params: params);
    if (dataState is DataSuccess){
      emit(VerifyAccountDone(dataState.data!));
    }
    if (dataState is DataFailed){
      emit(VerifyAccountError(dataState.error!));
    }
  }
}
