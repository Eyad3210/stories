class SignUpArgs{
  final String fullName;
  final String gender;
  final String work;
  final String birthday;

  SignUpArgs({required this.fullName, required this.gender, required this.work, required this.birthday});

}