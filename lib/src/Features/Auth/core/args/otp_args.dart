class OtpArgs {
  final String email;
  final String phone;
  final String token;

  OtpArgs({required this.email, required this.phone, required this.token});
}
