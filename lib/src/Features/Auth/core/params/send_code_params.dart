import 'package:equatable/equatable.dart';

class SendCodeParams extends Equatable {
  final String email;
  final String phone;

  const SendCodeParams({required this.email, required this.phone});

  Map<String, dynamic> toJson() {
    return {
      "email": email,
      "phone": '963$phone',
    };
  }

  @override
  List<Object> get props => [email, phone];
}