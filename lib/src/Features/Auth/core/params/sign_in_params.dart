import 'package:equatable/equatable.dart';

class SignInParams extends Equatable {
  final String phone;
  final String password;
  final String? deviceToken;

  const SignInParams({required this.phone, required this.password, this.deviceToken});

  Map<String, dynamic> toJson() {
    return {
      'phone': "963$phone",
      'password': password,
      'remember_token': deviceToken,
    };
  }

  @override
  List<Object> get props => [phone, password, deviceToken!];
}