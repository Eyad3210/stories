import 'package:equatable/equatable.dart';

class EditProfileParams extends Equatable {
  final String? name;
  final String? gender;
  final String? work;
  final String? phone;
  final String? email;
  final String? birthday;

  const EditProfileParams(
      {required this.name, required this.gender, required this.work, required this.phone, required this.email, required this.birthday});

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'gender': gender,
      'work': work,
      'birthday': birthday,
      'phone': phone != null ? "963$phone" : null,
      'email': email,
    };
  }

  @override
  List<Object> get props => [name!, gender!, work!, birthday!];
}