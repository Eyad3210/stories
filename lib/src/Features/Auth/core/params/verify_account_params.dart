import 'package:equatable/equatable.dart';

class VerifyAccountParams extends Equatable {
  final String code;
  final String email;
  final String phone;

  const VerifyAccountParams(
      {required this.code, required this.email, required this.phone});


  Map<String, dynamic> toJson() {
    return {
      "code": code,
      "email": email,
      "phone": '963$phone',
    };
  }

  @override
  List<Object> get props => [code, email, phone];
}