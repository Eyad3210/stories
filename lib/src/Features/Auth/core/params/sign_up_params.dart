import 'package:equatable/equatable.dart';

class SignUpParams extends Equatable {
  final String name;
  final String email;
  final String phone;
  final String password;
  final String gender;
  final String work;
  final String birthday;
  final String? deviceToken;

  const SignUpParams({required this.name,
    required this.email,
    required this.phone,
    required this.password,
    required this.gender,
    required this.work,
    required this.birthday,
    this.deviceToken});

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'email': email,
      'phone': "963$phone",
      'password': password,
      'gender': gender,
      'work': work,
      'birthday': birthday,
      'remember_token': deviceToken,
    };
  }


  @override
  String toString() {
    return 'SignUpParams{name: $name, email: $email, phone: $phone, password: $password, gender: $gender, work: $work, birthday: $birthday, deviceToken: $deviceToken}';
  }

  @override
  List<Object> get props =>
      [name, email, phone, password, gender, work, birthday, deviceToken!,];
}