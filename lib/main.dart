import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:stories/src/Shared/config/Translations/languages.dart';
import 'package:stories/src/Shared/config/router/app_router.dart';
import 'package:stories/src/Shared/config/theme/theme_service.dart';
import 'package:stories/src/Shared/config/theme/themes_app.dart';
import 'package:stories/src/injector.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDependencies();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  //runApp(const MyApp());
 runApp(DevicePreview(enabled: true, builder: (context) => const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return GetMaterialApp(
          transitionDuration: const Duration(milliseconds: 500),
          translations: Languages(),
          theme: ThemesApp.light,
          darkTheme: ThemesApp.dark,
          themeMode: ThemeService().theme,
          debugShowCheckedModeBanner: false,
          initialRoute: AppRouter.kSectionScreen,
          getPages: AppRouter.pages,
        );
      },
    );
  }
}
